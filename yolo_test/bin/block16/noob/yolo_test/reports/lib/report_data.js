var loopsJSON={
  "columns":["Pipelined", "II", "Bottleneck"]
  , "functions":
  [
    {
      "name":"Block4"
      , "data":
      ["Yes", "32758", "n/a"]
      , "debug":
      [
        [
          {
            "filename":"yolo_test.cl"
            , "line":60
            , "level":0
          }
        ]
      ]
    }
    , {
      "name":"Fully unrolled loop"
      , "data":
      ["n/a", "n/a", "n/a"]
      , "debug":
      [
        [
          {
            "filename":"yolo_test.cl"
            , "line":136
            , "level":1
          }
        ]
      ]
      , "details":
      [
        "Unrolled by #pragma unroll"
      ]
    }
  ]
}
;var mavJSON={
  "nodes":
  [
    {
      "type":"kernel"
      , "id":32
      , "name":"conv"
      , "file":""
      , "line":"0"
      , "children":[
        {
          "type":"bb"
          , "id":3
          , "name":"Block0.wii_blk"
          , "file":""
          , "line":"0"
          , "details":
          {
            "Latency":"3"
          }
        }
        , {
          "type":"bb"
          , "id":4
          , "name":"Block1"
          , "file":""
          , "line":"0"
          , "details":
          {
            "Latency":"7"
          }
        }
        , {
          "type":"bb"
          , "id":5
          , "name":"Block2"
          , "file":""
          , "line":"0"
          , "details":
          {
            "Latency":"2"
          }
        }
        , {
          "type":"bb"
          , "id":6
          , "name":"Block3"
          , "file":""
          , "line":"0"
          , "children":[
            {
              "type":"inst"
              , "id":8
              , "name":"Load"
              , "file":"1"
              , "line":"143"
              , "details":
              {
                "Width":"32 bits"
                , "Type":"Burst-coalesced"
                , "Stall-free":"No"
                , "Start-Cycle":"1"
                , "Latency":"160"
              }
            }
            , {
              "type":"inst"
              , "id":9
              , "name":"Load"
              , "file":"1"
              , "line":"144"
              , "details":
              {
                "Width":"32 bits"
                , "Type":"Burst-coalesced"
                , "Stall-free":"No"
                , "Start-Cycle":"1"
                , "Latency":"160"
              }
            }
            , {
              "type":"inst"
              , "id":10
              , "name":"Load"
              , "file":"1"
              , "line":"145"
              , "details":
              {
                "Width":"32 bits"
                , "Type":"Burst-coalesced"
                , "Stall-free":"No"
                , "Start-Cycle":"1"
                , "Latency":"160"
              }
            }
            , {
              "type":"inst"
              , "id":11
              , "name":"Store"
              , "file":"1"
              , "line":"143"
              , "details":
              {
                "Width":"32 bits"
                , "Type":"Pipelined"
                , "Stall-free":"Yes"
                , "Start-Cycle":"162"
                , "Latency":"2"
                , "Additional Info":" Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":12
              , "name":"Store"
              , "file":"1"
              , "line":"144"
              , "details":
              {
                "Width":"32 bits"
                , "Type":"Pipelined"
                , "Stall-free":"Yes"
                , "Start-Cycle":"162"
                , "Latency":"2"
                , "Additional Info":" Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":13
              , "name":"Store"
              , "file":"1"
              , "line":"145"
              , "details":
              {
                "Width":"32 bits"
                , "Type":"Pipelined"
                , "Stall-free":"Yes"
                , "Start-Cycle":"162"
                , "Latency":"2"
                , "Additional Info":" Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":14
              , "name":"Load"
              , "file":"1"
              , "line":"149"
              , "details":
              {
                "Width":"32 bits"
                , "Type":"Pipelined"
                , "Stall-free":"Yes"
                , "Start-Cycle":"163"
                , "Latency":"4"
                , "Additional Info":" Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":15
              , "name":"Load"
              , "file":"1"
              , "line":"149"
              , "details":
              {
                "Width":"32 bits"
                , "Type":"Pipelined"
                , "Stall-free":"Yes"
                , "Start-Cycle":"163"
                , "Latency":"4"
                , "Additional Info":" Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":16
              , "name":"Load"
              , "file":"1"
              , "line":"153"
              , "details":
              {
                "Width":"32 bits"
                , "Type":"Pipelined"
                , "Stall-free":"Yes"
                , "Start-Cycle":"163"
                , "Latency":"4"
                , "Additional Info":" Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":17
              , "name":"Store"
              , "file":"1"
              , "line":"160"
              , "details":
              {
                "Width":"64 bits"
                , "Type":"Burst-coalesced"
                , "Stall-free":"No"
                , "Start-Cycle":"685"
                , "Latency":"4"
              }
            }
            , {
              "type":"inst"
              , "id":18
              , "name":"end"
              , "file":"0"
              , "line":"0"
              , "details":
              {
                "Start-Cycle":"689"
                , "Latency":"1"
                , "Additional Info":"Exit from a basic block. Control flow branches at this node to one or more merge nodes. There is no control branching between merge and branch node for the same basic block."
              }
            }
            , {
              "type":"inst"
              , "id":19
              , "name":"begin"
              , "file":""
              , "line":""
              , "details":
              {
                "Start-Cycle":"0"
                , "Latency":"1"
                , "Additional Info":"Entrance to a basic block. Control flow comes to this node from one or more branch nodes, unless it's the very first merge node in a kernel. There is no control branching between merge and branch node within the same basic block."
              }
            }
          ]
          , "details":
          {
            "Latency":"690"
          }
        }
        , {
          "type":"bb"
          , "id":7
          , "name":"Block4"
          , "file":""
          , "line":"0"
          , "II":1
          , "LoopInfo":"Loop is not pipelined. See Optimization Report for more information."
          , "hasFmaxBottlenecks":"No"
          , "hasSubloops":"No"
          , "isPipelined":"No"
          , "children":[
            {
              "type":"inst"
              , "id":20
              , "name":"Load"
              , "file":"1"
              , "line":"119"
              , "details":
              {
                "Width":"32 bits"
                , "Type":"Burst-coalesced"
                , "Stall-free":"No"
                , "Start-Cycle":"1180"
                , "Latency":"160"
              }
            }
            , {
              "type":"inst"
              , "id":21
              , "name":"Load"
              , "file":"1"
              , "line":"119"
              , "details":
              {
                "Width":"32 bits"
                , "Type":"Burst-coalesced"
                , "Stall-free":"No"
                , "Start-Cycle":"1180"
                , "Latency":"160"
              }
            }
            , {
              "type":"inst"
              , "id":22
              , "name":"Load"
              , "file":"1"
              , "line":"123"
              , "details":
              {
                "Width":"64 bits"
                , "Type":"Burst-non-aligned"
                , "Stall-free":"No"
                , "Start-Cycle":"1471"
                , "Latency":"160"
              }
            }
            , {
              "type":"inst"
              , "id":23
              , "name":"Store"
              , "file":"1"
              , "line":"124"
              , "details":
              {
                "Width":"32 bits"
                , "Type":"Pipelined"
                , "Stall-free":"Yes"
                , "Start-Cycle":"1631"
                , "Latency":"2"
              }
            }
            , {
              "type":"inst"
              , "id":24
              , "name":"Store"
              , "file":"1"
              , "line":"124"
              , "details":
              {
                "Width":"32 bits"
                , "Type":"Pipelined"
                , "Stall-free":"Yes"
                , "Start-Cycle":"1631"
                , "Latency":"2"
              }
            }
            , {
              "type":"inst"
              , "id":25
              , "name":"Store"
              , "file":"1"
              , "line":"123"
              , "details":
              {
                "Width":"64 bits"
                , "Type":"Pipelined"
                , "Stall-free":"Yes"
                , "Start-Cycle":"1631"
                , "Latency":"2"
              }
            }
            , {
              "type":"inst"
              , "id":26
              , "name":"Load"
              , "file":"1"
              , "line":"137"
              , "details":
              {
                "Width":"512 bits"
                , "Type":"Pipelined"
                , "Stall-free":"Yes"
                , "Start-Cycle":"1765"
                , "Latency":"4"
                , "Additional Info":" Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":27
              , "name":"Load"
              , "file":"1"
              , "line":"137"
              , "details":
              {
                "Width":"512 bits"
                , "Type":"Pipelined"
                , "Stall-free":"Yes"
                , "Start-Cycle":"1765"
                , "Latency":"4"
                , "Additional Info":" Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":28
              , "name":"Load"
              , "file":"1"
              , "line":"137"
              , "details":
              {
                "Width":"512 bits"
                , "Type":"Pipelined"
                , "Stall-free":"Yes"
                , "Start-Cycle":"1765"
                , "Latency":"4"
                , "Additional Info":" Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":29
              , "name":"loop end"
              , "file":"1"
              , "line":"60"
              , "details":
              {
                "Start-Cycle":"2021"
                , "Latency":"1"
                , "Additional Info":"Exit from a basic block. Control flow branches at this node to one or more merge nodes. There is no control branching between merge and branch node for the same basic block."
              }
            }
            , {
              "type":"inst"
              , "id":30
              , "name":"loop"
              , "file":""
              , "line":""
              , "loopTo":29
              , "details":
              {
                "Start-Cycle":"0"
                , "Latency":"1"
                , "Additional Info":"Entrance to a basic block. Control flow comes to this node from one or more branch nodes, unless it's the very first merge node in a kernel. There is no control branching between merge and branch node within the same basic block."
              }
            }
          ]
          , "details":
          {
            "Latency":"2022"
          }
        }
        , {
          "type":"memtype"
          , "id":33
          , "name":"Local Memory"
          , "file":""
          , "line":"0"
          , "children":[
            {
              "type":"memsys"
              , "id":34
              , "name":"A_local"
              , "file":""
              , "line":"0"
              , "replFactor":"3"
              , "banks":1
              , "pumping":1
              , "children":[
                {
                  "type":"bank"
                  , "id":35
                  , "name":"Bank 0"
                  , "file":""
                  , "line":"0"
                }
              ]
            }
            , {
              "type":"memsys"
              , "id":36
              , "name":"B_local"
              , "file":""
              , "line":"0"
              , "replFactor":"3"
              , "banks":2
              , "pumping":1
              , "children":[
                {
                  "type":"bank"
                  , "id":37
                  , "name":"Bank 0"
                  , "file":""
                  , "line":"0"
                }
                , {
                  "type":"bank"
                  , "id":38
                  , "name":"Bank 1"
                  , "file":""
                  , "line":"0"
                }
              ]
            }
            , {
              "type":"memsys"
              , "id":39
              , "name":"mean_local"
              , "file":""
              , "line":"0"
              , "replFactor":"2"
              , "banks":1
              , "pumping":1
              , "children":[
                {
                  "type":"bank"
                  , "id":40
                  , "name":"Bank 0"
                  , "file":""
                  , "line":"0"
                }
              ]
            }
            , {
              "type":"memsys"
              , "id":41
              , "name":"variance_local"
              , "file":""
              , "line":"0"
              , "replFactor":"2"
              , "banks":1
              , "pumping":1
              , "children":[
                {
                  "type":"bank"
                  , "id":42
                  , "name":"Bank 0"
                  , "file":""
                  , "line":"0"
                }
              ]
            }
            , {
              "type":"memsys"
              , "id":43
              , "name":"bias_local"
              , "file":""
              , "line":"0"
              , "replFactor":"2"
              , "banks":1
              , "pumping":1
              , "children":[
                {
                  "type":"bank"
                  , "id":44
                  , "name":"Bank 0"
                  , "file":""
                  , "line":"0"
                }
              ]
            }
          ]
        }
      ]
    }
    , {
      "type":"memtype"
      , "id":45
      , "name":"Global Memory"
      , "file":""
      , "line":"0"
      , "children":[
        {
          "type":"memsys"
          , "id":46
          , "name":""
          , "file":""
          , "line":"0"
          , "replFactor":"0"
          , "banks":1
          , "pumping":0
          , "children":[
            {
              "type":"bank"
              , "id":47
              , "name":"Bank 0"
              , "file":""
              , "line":"0"
            }
          ]
        }
      ]
    }
  ]
  ,
  "links":
  [
    {
      "from":35
      , "to":26
    }
    ,
    {
      "from":25
      , "to":35
    }
    ,
    {
      "from":38
      , "to":27
    }
    ,
    {
      "from":37
      , "to":28
    }
    ,
    {
      "from":23
      , "to":38
    }
    ,
    {
      "from":24
      , "to":37
    }
    ,
    {
      "from":40
      , "to":14
    }
    ,
    {
      "from":11
      , "to":40
    }
    ,
    {
      "from":42
      , "to":15
    }
    ,
    {
      "from":12
      , "to":42
    }
    ,
    {
      "from":44
      , "to":16
    }
    ,
    {
      "from":13
      , "to":44
    }
    ,
    {
      "from":19
      , "to":9
    }
    ,
    {
      "from":22
      , "to":25
    }
    ,
    {
      "from":21
      , "to":25
    }
    ,
    {
      "from":20
      , "to":25
    }
    ,
    {
      "from":30
      , "to":21
    }
    ,
    {
      "from":8
      , "to":16
    }
    ,
    {
      "from":9
      , "to":16
    }
    ,
    {
      "from":10
      , "to":16
    }
    ,
    {
      "from":20
      , "to":23
    }
    ,
    {
      "from":21
      , "to":23
    }
    ,
    {
      "from":19
      , "to":10
    }
    ,
    {
      "from":8
      , "to":15
    }
    ,
    {
      "from":9
      , "to":15
    }
    ,
    {
      "from":10
      , "to":15
    }
    ,
    {
      "from":8
      , "to":12
    }
    ,
    {
      "from":9
      , "to":12
    }
    ,
    {
      "from":10
      , "to":12
    }
    ,
    {
      "from":25
      , "to":26
    }
    ,
    {
      "from":24
      , "to":26
    }
    ,
    {
      "from":23
      , "to":26
    }
    ,
    {
      "from":8
      , "to":11
    }
    ,
    {
      "from":9
      , "to":11
    }
    ,
    {
      "from":10
      , "to":11
    }
    ,
    {
      "from":8
      , "to":14
    }
    ,
    {
      "from":9
      , "to":14
    }
    ,
    {
      "from":10
      , "to":14
    }
    ,
    {
      "from":25
      , "to":27
    }
    ,
    {
      "from":24
      , "to":27
    }
    ,
    {
      "from":23
      , "to":27
    }
    ,
    {
      "from":21
      , "to":24
    }
    ,
    {
      "from":20
      , "to":24
    }
    ,
    {
      "from":19
      , "to":8
    }
    ,
    {
      "from":10
      , "to":17
    }
    ,
    {
      "from":9
      , "to":17
    }
    ,
    {
      "from":8
      , "to":17
    }
    ,
    {
      "from":16
      , "to":17
    }
    ,
    {
      "from":15
      , "to":17
    }
    ,
    {
      "from":14
      , "to":17
    }
    ,
    {
      "from":11
      , "to":17
    }
    ,
    {
      "from":12
      , "to":17
    }
    ,
    {
      "from":13
      , "to":17
    }
    ,
    {
      "from":25
      , "to":28
    }
    ,
    {
      "from":24
      , "to":28
    }
    ,
    {
      "from":23
      , "to":28
    }
    ,
    {
      "from":8
      , "to":13
    }
    ,
    {
      "from":9
      , "to":13
    }
    ,
    {
      "from":10
      , "to":13
    }
    ,
    {
      "from":30
      , "to":20
    }
    ,
    {
      "from":21
      , "to":22
    }
    ,
    {
      "from":20
      , "to":22
    }
    ,
    {
      "from":3
      , "to":4
    }
    ,
    {
      "from":17
      , "to":18
    }
    ,
    {
      "from":29
      , "to":30
    }
    ,
    {
      "from":5
      , "to":30
    }
    ,
    {
      "from":27
      , "to":29
    }
    ,
    {
      "from":26
      , "to":29
    }
    ,
    {
      "from":25
      , "to":29
    }
    ,
    {
      "from":24
      , "to":29
    }
    ,
    {
      "from":23
      , "to":29
    }
    ,
    {
      "from":28
      , "to":29
    }
    ,
    {
      "from":4
      , "to":19
    }
    ,
    {
      "from":29
      , "to":19
    }
    ,
    {
      "from":4
      , "to":5
    }
    ,
    {
      "from":47
      , "to":9
    }
    ,
    {
      "from":47
      , "to":21
    }
    ,
    {
      "from":47
      , "to":10
    }
    ,
    {
      "from":47
      , "to":8
    }
    ,
    {
      "from":17
      , "to":47
    }
    ,
    {
      "from":47
      , "to":20
    }
    ,
    {
      "from":47
      , "to":22
    }
  ]
  , "fileIndexMap":
  {
    "c:/Users/admin/Desktop/New folder/yolofpga/yolo_test/device/yolo_test.cl":"1"
  }
}
;var areaJSON={
  "columns":["ALUTs", "FFs", "RAMs", "DSPs"]
  , "debug_enabled":1
  , "total_percent":
  [92.8817, 58.2138, 39.8779, 87.9377, 83.9286]
  , "total":
  [63786, 87390, 452, 94]
  , "name":"Kernel System"
  , "max_resources":
  [109572, 219144, 514, 112]
  , "partitions":
  [
  ]
  , "resources":
  [
    {
      "name":"Board interface"
      , "data":
      [2160, 1908, 20, 0]
      , "details":
      [
        "Platform interface logic."
      ]
    }
    , {
      "name":"Global interconnect"
      , "data":
      [9588, 10682, 0, 0]
      , "details":
      [
        "Global interconnect for 6 global loads and 1 global store. Reduce number of global loads and stores to simplify global interconnect."
      ]
    }
  ]
  , "functions":
  [
    {
      "name":"conv"
      , "compute_units":1
      , "details":
      [
        "Achieved kernel vectorization: 2"
        , "Number of compute units: 1"
      ]
      , "resources":
      [
        {
          "name":"Function overhead"
          , "data":
          [1814, 1816, 0, 0]
          , "details":
          [
            "Kernel dispatch logic."
          ]
        }
        , {
          "name":"yolo_test.cl:32 (A_local)"
          , "data":
          [0, 0, 13, 0]
          , "debug":
          [
            [
              {
                "filename":"c:/Users/admin/Desktop/New folder/yolofpga/yolo_test/device/yolo_test.cl"
                , "line":32
              }
            ]
          ]
          , "details":
          [
            "Local memory: Optimal.\nRequested size 1024 bytes (rounded up to nearest power of 2), implemented size 3072 bytes, replicated 3 times total, stall-free, 1 read and 1 write. Additional information:\n- Replicated 3 times to efficiently support multiple simultaneous workgroups. This replication resulted in no increase in actual block RAM usage."
          ]
        }
        , {
          "name":"yolo_test.cl:33 (B_local)"
          , "data":
          [0, 0, 26, 0]
          , "debug":
          [
            [
              {
                "filename":"c:/Users/admin/Desktop/New folder/yolofpga/yolo_test/device/yolo_test.cl"
                , "line":33
              }
            ]
          ]
          , "details":
          [
            "Local memory: Optimal.\nRequested size 1024 bytes (rounded up to nearest power of 2), implemented size 3072 bytes, replicated 3 times total, stall-free, 2 reads and 2 writes. Additional information:\n- Replicated 3 times to efficiently support multiple simultaneous workgroups. This replication resulted in no increase in actual block RAM usage.\n- Banked on lowest dimension into 2 separate banks (this is a good thing)."
          ]
        }
        , {
          "name":"yolo_test.cl:34 (mean_local)"
          , "data":
          [0, 0, 1, 0]
          , "debug":
          [
            [
              {
                "filename":"c:/Users/admin/Desktop/New folder/yolofpga/yolo_test/device/yolo_test.cl"
                , "line":34
              }
            ]
          ]
          , "details":
          [
            "Local memory: Optimal.\nRequested size 4 bytes (rounded up to nearest power of 2), implemented size 8 bytes, replicated 2 times total, stall-free, 1 read and 1 write. Additional information:\n- Replicated 2 times to efficiently support multiple simultaneous workgroups. This replication resulted in no increase in actual block RAM usage."
          ]
        }
        , {
          "name":"yolo_test.cl:35 (bias_local)"
          , "data":
          [0, 0, 1, 0]
          , "debug":
          [
            [
              {
                "filename":"c:/Users/admin/Desktop/New folder/yolofpga/yolo_test/device/yolo_test.cl"
                , "line":35
              }
            ]
          ]
          , "details":
          [
            "Local memory: Optimal.\nRequested size 4 bytes (rounded up to nearest power of 2), implemented size 8 bytes, replicated 2 times total, stall-free, 1 read and 1 write. Additional information:\n- Replicated 2 times to efficiently support multiple simultaneous workgroups. This replication resulted in no increase in actual block RAM usage."
          ]
        }
        , {
          "name":"yolo_test.cl:36 (variance_local)"
          , "data":
          [0, 0, 1, 0]
          , "debug":
          [
            [
              {
                "filename":"c:/Users/admin/Desktop/New folder/yolofpga/yolo_test/device/yolo_test.cl"
                , "line":36
              }
            ]
          ]
          , "details":
          [
            "Local memory: Optimal.\nRequested size 4 bytes (rounded up to nearest power of 2), implemented size 8 bytes, replicated 2 times total, stall-free, 1 read and 1 write. Additional information:\n- Replicated 2 times to efficiently support multiple simultaneous workgroups. This replication resulted in no increase in actual block RAM usage."
          ]
        }
      ]
      , "basicblocks":
      [
        {
          "name":"Block0.wii_blk"
          , "resources":
          [
            {
              "name":"State"
              , "data":
              [224, 224, 0, 0]
              , "details":
              [
                "Resources for live values and control logic. To reduce this area:\n- reduce size of local variables\n- reduce scope of local variables, localizing them as much as possible\n- reduce number of nested loops"
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Control flow logic"
                    , "data":
                    [224, 224, 0, 0]
                  }
                  , "count":0
                }
              ]
            }
          ]
          , "computation":
          [
          ]
        }
        , {
          "name":"Block1"
          , "resources":
          [
            {
              "name":"State"
              , "data":
              [265, 1413, 0, 0]
              , "details":
              [
                "Resources for live values and control logic. To reduce this area:\n- reduce size of local variables\n- reduce scope of local variables, localizing them as much as possible\n- reduce number of nested loops"
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Control flow logic"
                    , "data":
                    [224, 569, 0, 0]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"No Source Line"
                    , "data":
                    [0, 39.75, 0, 0]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"yolo_test.cl:123"
                    , "data":
                    [13.3333, 217.083, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"c:/Users/admin/Desktop/New folder/yolofpga/yolo_test/device/yolo_test.cl"
                          , "line":123
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"yolo_test.cl:60"
                    , "data":
                    [14.3333, 370.083, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"c:/Users/admin/Desktop/New folder/yolofpga/yolo_test/device/yolo_test.cl"
                          , "line":60
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"yolo_test.cl:69"
                    , "data":
                    [13.3333, 217.083, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"c:/Users/admin/Desktop/New folder/yolofpga/yolo_test/device/yolo_test.cl"
                          , "line":69
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
              ]
            }
          ]
          , "computation":
          [
            {
              "name":"yolo_test.cl:51"
              , "data":
              [0, 28, 0, 2]
              , "debug":
              [
                [
                  {
                    "filename":"c:/Users/admin/Desktop/New folder/yolofpga/yolo_test/device/yolo_test.cl"
                    , "line":51
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Mul"
                    , "data":
                    [0, 28, 0, 2]
                  }
                  , "count":1
                }
              ]
            }
            , {
              "name":"yolo_test.cl:52"
              , "data":
              [14, 0, 0, 0]
              , "debug":
              [
                [
                  {
                    "filename":"c:/Users/admin/Desktop/New folder/yolofpga/yolo_test/device/yolo_test.cl"
                    , "line":52
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Add"
                    , "data":
                    [14, 0, 0, 0]
                  }
                  , "count":1
                }
              ]
            }
            , {
              "name":"yolo_test.cl:60"
              , "data":
              [16, 0, 0, 0]
              , "debug":
              [
                [
                  {
                    "filename":"c:/Users/admin/Desktop/New folder/yolofpga/yolo_test/device/yolo_test.cl"
                    , "line":60
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Integer Compare"
                    , "data":
                    [16, 0, 0, 0]
                  }
                  , "count":1
                }
              ]
            }
            , {
              "name":"yolo_test.cl:69"
              , "data":
              [30, 32, 0, 2]
              , "debug":
              [
                [
                  {
                    "filename":"c:/Users/admin/Desktop/New folder/yolofpga/yolo_test/device/yolo_test.cl"
                    , "line":69
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Add"
                    , "data":
                    [30, 0, 0, 0]
                  }
                  , "count":2
                }
                , {
                  "info":
                  {
                    "name":"Mul"
                    , "data":
                    [0, 32, 0, 2]
                  }
                  , "count":1
                }
              ]
            }
            , {
              "name":"yolo_test.cl:123"
              , "data":
              [15, 32, 0, 2]
              , "debug":
              [
                [
                  {
                    "filename":"c:/Users/admin/Desktop/New folder/yolofpga/yolo_test/device/yolo_test.cl"
                    , "line":123
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Add"
                    , "data":
                    [15, 0, 0, 0]
                  }
                  , "count":1
                }
                , {
                  "info":
                  {
                    "name":"Mul"
                    , "data":
                    [0, 32, 0, 2]
                  }
                  , "count":1
                }
              ]
            }
          ]
        }
        , {
          "name":"Block2"
          , "resources":
          [
            {
              "name":"State"
              , "data":
              [378, 567, 0, 0]
              , "details":
              [
                "Resources for live values and control logic. To reduce this area:\n- reduce size of local variables\n- reduce scope of local variables, localizing them as much as possible\n- reduce number of nested loops"
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Control flow logic"
                    , "data":
                    [378, 567, 0, 0]
                  }
                  , "count":0
                }
              ]
            }
          ]
          , "computation":
          [
          ]
        }
        , {
          "name":"Block3"
          , "resources":
          [
            {
              "name":"State"
              , "data":
              [2208.3, 5033.81, 12.5, 0]
              , "details":
              [
                "Resources for live values and control logic. To reduce this area:\n- reduce size of local variables\n- reduce scope of local variables, localizing them as much as possible\n- reduce number of nested loops"
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Control flow logic"
                    , "data":
                    [162, 602, 0, 0]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"No Source Line"
                    , "data":
                    [549, 1269, 9, 0]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"yolo_test.cl:143"
                    , "data":
                    [62.2, 62.2, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"c:/Users/admin/Desktop/New folder/yolofpga/yolo_test/device/yolo_test.cl"
                          , "line":143
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"yolo_test.cl:144"
                    , "data":
                    [62.2, 62.2, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"c:/Users/admin/Desktop/New folder/yolofpga/yolo_test/device/yolo_test.cl"
                          , "line":144
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"yolo_test.cl:145"
                    , "data":
                    [62.2, 62.2, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"c:/Users/admin/Desktop/New folder/yolofpga/yolo_test/device/yolo_test.cl"
                          , "line":145
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"yolo_test.cl:146"
                    , "data":
                    [48, 48, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"c:/Users/admin/Desktop/New folder/yolofpga/yolo_test/device/yolo_test.cl"
                          , "line":146
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"yolo_test.cl:148"
                    , "data":
                    [0, 1, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"c:/Users/admin/Desktop/New folder/yolofpga/yolo_test/device/yolo_test.cl"
                          , "line":148
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"yolo_test.cl:149"
                    , "data":
                    [700, 1452, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"c:/Users/admin/Desktop/New folder/yolofpga/yolo_test/device/yolo_test.cl"
                          , "line":149
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"yolo_test.cl:153"
                    , "data":
                    [441, 910, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"c:/Users/admin/Desktop/New folder/yolofpga/yolo_test/device/yolo_test.cl"
                          , "line":153
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"yolo_test.cl:160"
                    , "data":
                    [121.7, 565.2, 3.5, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"c:/Users/admin/Desktop/New folder/yolofpga/yolo_test/device/yolo_test.cl"
                          , "line":160
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
              ]
            }
            , {
              "name":"Cluster logic"
              , "data":
              [324.7, 716.2, 2.5, 0]
              , "details":
              [
                "Logic required to efficiently support sets of operations that do not stall. This area cannot be affected directly."
              ]
            }
          ]
          , "computation":
          [
            {
              "name":"No Source Line"
              , "data":
              [68, 96, 0, 2]
              , "debug":
              [
                [
                  {
                    "filename":""
                    , "line":0
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Fmul"
                    , "data":
                    [68, 96, 0, 2]
                  }
                  , "count":2
                }
              ]
            }
            , {
              "name":"yolo_test.cl:143"
              , "data":
              [3623, 4866, 43, 0]
              , "debug":
              [
                [
                  {
                    "filename":"c:/Users/admin/Desktop/New folder/yolofpga/yolo_test/device/yolo_test.cl"
                    , "line":143
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Load"
                    , "data":
                    [3445, 4714, 43, 0]
                    , "details":
                    [
                      "Load with a private 64 kilobit cache. Cache is not shared with any other load. It is flushed on kernel start. Use Dynamic Profiler to verify cache effectiveness. Other kernels should not be updating the data in global memory while this kernel is using it. Cache is created when memory access pattern is data-dependent or appears to be repetitive. Simplify access pattern or mark pointer as 'volatile' to disable generation of this cache."
                    ]
                  }
                  , "count":1
                }
                , {
                  "info":
                  {
                    "name":"Pointer Computation"
                    , "data":
                    [16, 0, 0, 0]
                  }
                  , "count":1
                }
                , {
                  "info":
                  {
                    "name":"Store"
                    , "data":
                    [162, 152, 0, 0]
                    , "details":
                    [
                      "Stall-free write to memory declared on yolo_test.cl:34."
                    ]
                  }
                  , "count":1
                }
              ]
            }
            , {
              "name":"yolo_test.cl:144"
              , "data":
              [3623, 4866, 43, 0]
              , "debug":
              [
                [
                  {
                    "filename":"c:/Users/admin/Desktop/New folder/yolofpga/yolo_test/device/yolo_test.cl"
                    , "line":144
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Load"
                    , "data":
                    [3445, 4714, 43, 0]
                    , "details":
                    [
                      "Load with a private 64 kilobit cache. Cache is not shared with any other load. It is flushed on kernel start. Use Dynamic Profiler to verify cache effectiveness. Other kernels should not be updating the data in global memory while this kernel is using it. Cache is created when memory access pattern is data-dependent or appears to be repetitive. Simplify access pattern or mark pointer as 'volatile' to disable generation of this cache."
                    ]
                  }
                  , "count":1
                }
                , {
                  "info":
                  {
                    "name":"Pointer Computation"
                    , "data":
                    [16, 0, 0, 0]
                  }
                  , "count":1
                }
                , {
                  "info":
                  {
                    "name":"Store"
                    , "data":
                    [162, 152, 0, 0]
                    , "details":
                    [
                      "Stall-free write to memory declared on yolo_test.cl:36."
                    ]
                  }
                  , "count":1
                }
              ]
            }
            , {
              "name":"yolo_test.cl:145"
              , "data":
              [3623, 4866, 43, 0]
              , "debug":
              [
                [
                  {
                    "filename":"c:/Users/admin/Desktop/New folder/yolofpga/yolo_test/device/yolo_test.cl"
                    , "line":145
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Load"
                    , "data":
                    [3445, 4714, 43, 0]
                    , "details":
                    [
                      "Load with a private 64 kilobit cache. Cache is not shared with any other load. It is flushed on kernel start. Use Dynamic Profiler to verify cache effectiveness. Other kernels should not be updating the data in global memory while this kernel is using it. Cache is created when memory access pattern is data-dependent or appears to be repetitive. Simplify access pattern or mark pointer as 'volatile' to disable generation of this cache."
                    ]
                  }
                  , "count":1
                }
                , {
                  "info":
                  {
                    "name":"Pointer Computation"
                    , "data":
                    [16, 0, 0, 0]
                  }
                  , "count":1
                }
                , {
                  "info":
                  {
                    "name":"Store"
                    , "data":
                    [162, 152, 0, 0]
                    , "details":
                    [
                      "Stall-free write to memory declared on yolo_test.cl:35."
                    ]
                  }
                  , "count":1
                }
              ]
            }
            , {
              "name":"yolo_test.cl:146"
              , "data":
              [110, 73, 0, 0]
              , "debug":
              [
                [
                  {
                    "filename":"c:/Users/admin/Desktop/New folder/yolofpga/yolo_test/device/yolo_test.cl"
                    , "line":146
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"'__acl_barrier' Function Call"
                    , "data":
                    [110, 73, 0, 0]
                  }
                  , "count":1
                }
              ]
            }
            , {
              "name":"yolo_test.cl:149"
              , "data":
              [624, 368, 0, 2]
              , "debug":
              [
                [
                  {
                    "filename":"c:/Users/admin/Desktop/New folder/yolofpga/yolo_test/device/yolo_test.cl"
                    , "line":149
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Fmul"
                    , "data":
                    [68, 96, 0, 2]
                  }
                  , "count":2
                }
                , {
                  "info":
                  {
                    "name":"Fsub"
                    , "data":
                    [282, 0, 0, 0]
                  }
                  , "count":2
                }
                , {
                  "info":
                  {
                    "name":"Load"
                    , "data":
                    [274, 272, 0, 0]
                    , "details":
                    [
                      "Stall-free read from memory declared on yolo_test.cl:34."
                      , "Stall-free read from memory declared on yolo_test.cl:36."
                    ]
                  }
                  , "count":2
                }
              ]
            }
            , {
              "name":"yolo_test.cl:151"
              , "data":
              [110, 73, 0, 0]
              , "debug":
              [
                [
                  {
                    "filename":"c:/Users/admin/Desktop/New folder/yolofpga/yolo_test/device/yolo_test.cl"
                    , "line":151
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"'__acl_barrier' Function Call"
                    , "data":
                    [110, 73, 0, 0]
                  }
                  , "count":1
                }
              ]
            }
            , {
              "name":"yolo_test.cl:153"
              , "data":
              [419, 136, 0, 0]
              , "debug":
              [
                [
                  {
                    "filename":"c:/Users/admin/Desktop/New folder/yolofpga/yolo_test/device/yolo_test.cl"
                    , "line":153
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Fadd"
                    , "data":
                    [282, 0, 0, 0]
                  }
                  , "count":2
                }
                , {
                  "info":
                  {
                    "name":"Load"
                    , "data":
                    [137, 136, 0, 0]
                    , "details":
                    [
                      "Stall-free read from memory declared on yolo_test.cl:35."
                    ]
                  }
                  , "count":1
                }
              ]
            }
            , {
              "name":"yolo_test.cl:154"
              , "data":
              [110, 73, 0, 0]
              , "debug":
              [
                [
                  {
                    "filename":"c:/Users/admin/Desktop/New folder/yolofpga/yolo_test/device/yolo_test.cl"
                    , "line":154
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"'__acl_barrier' Function Call"
                    , "data":
                    [110, 73, 0, 0]
                  }
                  , "count":1
                }
              ]
            }
            , {
              "name":"yolo_test.cl:156"
              , "data":
              [126, 56, 0, 0]
              , "debug":
              [
                [
                  {
                    "filename":"c:/Users/admin/Desktop/New folder/yolofpga/yolo_test/device/yolo_test.cl"
                    , "line":156
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Floating Point Compare"
                    , "data":
                    [126, 56, 0, 0]
                  }
                  , "count":2
                }
              ]
            }
            , {
              "name":"yolo_test.cl:159"
              , "data":
              [110, 73, 0, 0]
              , "debug":
              [
                [
                  {
                    "filename":"c:/Users/admin/Desktop/New folder/yolofpga/yolo_test/device/yolo_test.cl"
                    , "line":159
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"'__acl_barrier' Function Call"
                    , "data":
                    [110, 73, 0, 0]
                  }
                  , "count":1
                }
              ]
            }
            , {
              "name":"yolo_test.cl:160"
              , "data":
              [784, 2619, 16, 2]
              , "debug":
              [
                [
                  {
                    "filename":"c:/Users/admin/Desktop/New folder/yolofpga/yolo_test/device/yolo_test.cl"
                    , "line":160
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Add"
                    , "data":
                    [15, 0, 0, 0]
                  }
                  , "count":1
                }
                , {
                  "info":
                  {
                    "name":"Mul"
                    , "data":
                    [0, 63, 0, 2]
                  }
                  , "count":1
                }
                , {
                  "info":
                  {
                    "name":"Pointer Computation"
                    , "data":
                    [31, 0, 0, 0]
                  }
                  , "count":1
                }
                , {
                  "info":
                  {
                    "name":"Store"
                    , "data":
                    [738, 2556, 16, 0]
                  }
                  , "count":1
                }
              ]
            }
          ]
        }
        , {
          "name":"Block4"
          , "resources":
          [
            {
              "name":"State"
              , "data":
              [11088.5, 23646.3, 87, 0]
              , "details":
              [
                "Resources for live values and control logic. To reduce this area:\n- reduce size of local variables\n- reduce scope of local variables, localizing them as much as possible\n- reduce number of nested loops"
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Control flow logic"
                    , "data":
                    [438, 1031, 0, 0]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"No Source Line"
                    , "data":
                    [290, 663, 12, 0]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"yolo_test.cl:105"
                    , "data":
                    [61, 151.5, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"c:/Users/admin/Desktop/New folder/yolofpga/yolo_test/device/yolo_test.cl"
                          , "line":105
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"yolo_test.cl:107"
                    , "data":
                    [70.3333, 156.167, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"c:/Users/admin/Desktop/New folder/yolofpga/yolo_test/device/yolo_test.cl"
                          , "line":107
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"yolo_test.cl:108"
                    , "data":
                    [143.2, 244.4, 2, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"c:/Users/admin/Desktop/New folder/yolofpga/yolo_test/device/yolo_test.cl"
                          , "line":108
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"yolo_test.cl:109"
                    , "data":
                    [46, 71, 1, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"c:/Users/admin/Desktop/New folder/yolofpga/yolo_test/device/yolo_test.cl"
                          , "line":109
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"yolo_test.cl:111"
                    , "data":
                    [32, 64, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"c:/Users/admin/Desktop/New folder/yolofpga/yolo_test/device/yolo_test.cl"
                          , "line":111
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"yolo_test.cl:112"
                    , "data":
                    [160, 320, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"c:/Users/admin/Desktop/New folder/yolofpga/yolo_test/device/yolo_test.cl"
                          , "line":112
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"yolo_test.cl:115"
                    , "data":
                    [261.733, 472.467, 3, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"c:/Users/admin/Desktop/New folder/yolofpga/yolo_test/device/yolo_test.cl"
                          , "line":115
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"yolo_test.cl:119"
                    , "data":
                    [394.667, 551.333, 2, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"c:/Users/admin/Desktop/New folder/yolofpga/yolo_test/device/yolo_test.cl"
                          , "line":119
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"yolo_test.cl:122"
                    , "data":
                    [32, 32, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"c:/Users/admin/Desktop/New folder/yolofpga/yolo_test/device/yolo_test.cl"
                          , "line":122
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"yolo_test.cl:123"
                    , "data":
                    [212.167, 477.833, 9.5, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"c:/Users/admin/Desktop/New folder/yolofpga/yolo_test/device/yolo_test.cl"
                          , "line":123
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"yolo_test.cl:124"
                    , "data":
                    [144.333, 309.667, 2, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"c:/Users/admin/Desktop/New folder/yolofpga/yolo_test/device/yolo_test.cl"
                          , "line":124
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"yolo_test.cl:137"
                    , "data":
                    [7938, 16620.9, 28, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"c:/Users/admin/Desktop/New folder/yolofpga/yolo_test/device/yolo_test.cl"
                          , "line":137
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"yolo_test.cl:60"
                    , "data":
                    [327, 1448.67, 23.5, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"c:/Users/admin/Desktop/New folder/yolofpga/yolo_test/device/yolo_test.cl"
                          , "line":60
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"yolo_test.cl:70"
                    , "data":
                    [25, 19, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"c:/Users/admin/Desktop/New folder/yolofpga/yolo_test/device/yolo_test.cl"
                          , "line":70
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"yolo_test.cl:74"
                    , "data":
                    [54, 148, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"c:/Users/admin/Desktop/New folder/yolofpga/yolo_test/device/yolo_test.cl"
                          , "line":74
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"yolo_test.cl:76"
                    , "data":
                    [65.2, 109.4, 1, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"c:/Users/admin/Desktop/New folder/yolofpga/yolo_test/device/yolo_test.cl"
                          , "line":76
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"yolo_test.cl:80"
                    , "data":
                    [19.2, 38.4, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"c:/Users/admin/Desktop/New folder/yolofpga/yolo_test/device/yolo_test.cl"
                          , "line":80
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"yolo_test.cl:84"
                    , "data":
                    [46, 71, 1, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"c:/Users/admin/Desktop/New folder/yolofpga/yolo_test/device/yolo_test.cl"
                          , "line":84
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"yolo_test.cl:88"
                    , "data":
                    [70.3333, 156.167, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"c:/Users/admin/Desktop/New folder/yolofpga/yolo_test/device/yolo_test.cl"
                          , "line":88
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"yolo_test.cl:90"
                    , "data":
                    [70.3333, 156.167, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"c:/Users/admin/Desktop/New folder/yolofpga/yolo_test/device/yolo_test.cl"
                          , "line":90
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"yolo_test.cl:94"
                    , "data":
                    [92, 142, 2, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"c:/Users/admin/Desktop/New folder/yolofpga/yolo_test/device/yolo_test.cl"
                          , "line":94
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"yolo_test.cl:96"
                    , "data":
                    [96, 192, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"c:/Users/admin/Desktop/New folder/yolofpga/yolo_test/device/yolo_test.cl"
                          , "line":96
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
              ]
            }
            , {
              "name":"Cluster logic"
              , "data":
              [871.5, 2592.83, 13, 0]
              , "details":
              [
                "Logic required to efficiently support sets of operations that do not stall. This area cannot be affected directly."
              ]
            }
          ]
          , "computation":
          [
            {
              "name":"yolo_test.cl:60"
              , "data":
              [30, 0, 0, 0]
              , "debug":
              [
                [
                  {
                    "filename":"c:/Users/admin/Desktop/New folder/yolofpga/yolo_test/device/yolo_test.cl"
                    , "line":60
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Add"
                    , "data":
                    [14, 0, 0, 0]
                  }
                  , "count":2
                }
                , {
                  "info":
                  {
                    "name":"Integer Compare"
                    , "data":
                    [16, 0, 0, 0]
                  }
                  , "count":1
                }
              ]
            }
            , {
              "name":"yolo_test.cl:69"
              , "data":
              [28, 0, 0, 0]
              , "debug":
              [
                [
                  {
                    "filename":"c:/Users/admin/Desktop/New folder/yolofpga/yolo_test/device/yolo_test.cl"
                    , "line":69
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Add"
                    , "data":
                    [28, 0, 0, 0]
                  }
                  , "count":2
                }
              ]
            }
            , {
              "name":"yolo_test.cl:70"
              , "data":
              [110, 73, 1, 0]
              , "debug":
              [
                [
                  {
                    "filename":"c:/Users/admin/Desktop/New folder/yolofpga/yolo_test/device/yolo_test.cl"
                    , "line":70
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"'__acl_barrier' Function Call"
                    , "data":
                    [110, 73, 1, 0]
                  }
                  , "count":1
                }
              ]
            }
            , {
              "name":"yolo_test.cl:74"
              , "data":
              [644, 838, 0, 2]
              , "debug":
              [
                [
                  {
                    "filename":"c:/Users/admin/Desktop/New folder/yolofpga/yolo_test/device/yolo_test.cl"
                    , "line":74
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Floating Point to Integer Conversion"
                    , "data":
                    [232, 228, 0, 0]
                  }
                  , "count":2
                }
                , {
                  "info":
                  {
                    "name":"Fmul"
                    , "data":
                    [68, 96, 0, 2]
                  }
                  , "count":2
                }
                , {
                  "info":
                  {
                    "name":"Integer to Floating Point Conversion"
                    , "data":
                    [344, 514, 0, 0]
                  }
                  , "count":2
                }
              ]
            }
            , {
              "name":"yolo_test.cl:76"
              , "data":
              [32, 64, 0, 4]
              , "debug":
              [
                [
                  {
                    "filename":"c:/Users/admin/Desktop/New folder/yolofpga/yolo_test/device/yolo_test.cl"
                    , "line":76
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Mul"
                    , "data":
                    [0, 64, 0, 4]
                  }
                  , "count":2
                }
                , {
                  "info":
                  {
                    "name":"Sub"
                    , "data":
                    [32, 0, 0, 0]
                  }
                  , "count":2
                }
              ]
            }
            , {
              "name":"yolo_test.cl:77"
              , "data":
              [110, 73, 0, 0]
              , "debug":
              [
                [
                  {
                    "filename":"c:/Users/admin/Desktop/New folder/yolofpga/yolo_test/device/yolo_test.cl"
                    , "line":77
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"'__acl_barrier' Function Call"
                    , "data":
                    [110, 73, 0, 0]
                  }
                  , "count":1
                }
              ]
            }
            , {
              "name":"yolo_test.cl:80"
              , "data":
              [0, 64, 0, 4]
              , "debug":
              [
                [
                  {
                    "filename":"c:/Users/admin/Desktop/New folder/yolofpga/yolo_test/device/yolo_test.cl"
                    , "line":80
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Mul"
                    , "data":
                    [0, 64, 0, 4]
                  }
                  , "count":2
                }
              ]
            }
            , {
              "name":"yolo_test.cl:81"
              , "data":
              [110, 73, 0, 0]
              , "debug":
              [
                [
                  {
                    "filename":"c:/Users/admin/Desktop/New folder/yolofpga/yolo_test/device/yolo_test.cl"
                    , "line":81
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"'__acl_barrier' Function Call"
                    , "data":
                    [110, 73, 0, 0]
                  }
                  , "count":1
                }
              ]
            }
            , {
              "name":"yolo_test.cl:84"
              , "data":
              [32, 0, 0, 0]
              , "debug":
              [
                [
                  {
                    "filename":"c:/Users/admin/Desktop/New folder/yolofpga/yolo_test/device/yolo_test.cl"
                    , "line":84
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Sub"
                    , "data":
                    [32, 0, 0, 0]
                  }
                  , "count":2
                }
              ]
            }
            , {
              "name":"yolo_test.cl:85"
              , "data":
              [110, 73, 0, 0]
              , "debug":
              [
                [
                  {
                    "filename":"c:/Users/admin/Desktop/New folder/yolofpga/yolo_test/device/yolo_test.cl"
                    , "line":85
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"'__acl_barrier' Function Call"
                    , "data":
                    [110, 73, 0, 0]
                  }
                  , "count":1
                }
              ]
            }
            , {
              "name":"yolo_test.cl:88"
              , "data":
              [644, 838, 0, 2]
              , "debug":
              [
                [
                  {
                    "filename":"c:/Users/admin/Desktop/New folder/yolofpga/yolo_test/device/yolo_test.cl"
                    , "line":88
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Floating Point to Integer Conversion"
                    , "data":
                    [232, 228, 0, 0]
                  }
                  , "count":2
                }
                , {
                  "info":
                  {
                    "name":"Fmul"
                    , "data":
                    [68, 96, 0, 2]
                  }
                  , "count":2
                }
                , {
                  "info":
                  {
                    "name":"Integer to Floating Point Conversion"
                    , "data":
                    [344, 514, 0, 0]
                  }
                  , "count":2
                }
              ]
            }
            , {
              "name":"yolo_test.cl:90"
              , "data":
              [300, 324, 0, 2]
              , "debug":
              [
                [
                  {
                    "filename":"c:/Users/admin/Desktop/New folder/yolofpga/yolo_test/device/yolo_test.cl"
                    , "line":90
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Floating Point to Integer Conversion"
                    , "data":
                    [232, 228, 0, 0]
                  }
                  , "count":2
                }
                , {
                  "info":
                  {
                    "name":"Fmul"
                    , "data":
                    [68, 96, 0, 2]
                  }
                  , "count":2
                }
              ]
            }
            , {
              "name":"yolo_test.cl:91"
              , "data":
              [110, 73, 0, 0]
              , "debug":
              [
                [
                  {
                    "filename":"c:/Users/admin/Desktop/New folder/yolofpga/yolo_test/device/yolo_test.cl"
                    , "line":91
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"'__acl_barrier' Function Call"
                    , "data":
                    [110, 73, 0, 0]
                  }
                  , "count":1
                }
              ]
            }
            , {
              "name":"yolo_test.cl:94"
              , "data":
              [32, 64, 0, 4]
              , "debug":
              [
                [
                  {
                    "filename":"c:/Users/admin/Desktop/New folder/yolofpga/yolo_test/device/yolo_test.cl"
                    , "line":94
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Mul"
                    , "data":
                    [0, 64, 0, 4]
                  }
                  , "count":2
                }
                , {
                  "info":
                  {
                    "name":"Sub"
                    , "data":
                    [32, 0, 0, 0]
                  }
                  , "count":2
                }
              ]
            }
            , {
              "name":"yolo_test.cl:96"
              , "data":
              [32, 64, 0, 4]
              , "debug":
              [
                [
                  {
                    "filename":"c:/Users/admin/Desktop/New folder/yolofpga/yolo_test/device/yolo_test.cl"
                    , "line":96
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Mul"
                    , "data":
                    [0, 64, 0, 4]
                  }
                  , "count":2
                }
                , {
                  "info":
                  {
                    "name":"Sub"
                    , "data":
                    [32, 0, 0, 0]
                  }
                  , "count":2
                }
              ]
            }
            , {
              "name":"yolo_test.cl:97"
              , "data":
              [110, 73, 0, 0]
              , "debug":
              [
                [
                  {
                    "filename":"c:/Users/admin/Desktop/New folder/yolofpga/yolo_test/device/yolo_test.cl"
                    , "line":97
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"'__acl_barrier' Function Call"
                    , "data":
                    [110, 73, 0, 0]
                  }
                  , "count":1
                }
              ]
            }
            , {
              "name":"yolo_test.cl:103"
              , "data":
              [110, 73, 0, 0]
              , "debug":
              [
                [
                  {
                    "filename":"c:/Users/admin/Desktop/New folder/yolofpga/yolo_test/device/yolo_test.cl"
                    , "line":103
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"'__acl_barrier' Function Call"
                    , "data":
                    [110, 73, 0, 0]
                  }
                  , "count":1
                }
              ]
            }
            , {
              "name":"yolo_test.cl:105"
              , "data":
              [644, 838, 0, 2]
              , "debug":
              [
                [
                  {
                    "filename":"c:/Users/admin/Desktop/New folder/yolofpga/yolo_test/device/yolo_test.cl"
                    , "line":105
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Floating Point to Integer Conversion"
                    , "data":
                    [232, 228, 0, 0]
                  }
                  , "count":2
                }
                , {
                  "info":
                  {
                    "name":"Fmul"
                    , "data":
                    [68, 96, 0, 2]
                  }
                  , "count":2
                }
                , {
                  "info":
                  {
                    "name":"Integer to Floating Point Conversion"
                    , "data":
                    [344, 514, 0, 0]
                  }
                  , "count":2
                }
              ]
            }
            , {
              "name":"yolo_test.cl:107"
              , "data":
              [644, 838, 0, 2]
              , "debug":
              [
                [
                  {
                    "filename":"c:/Users/admin/Desktop/New folder/yolofpga/yolo_test/device/yolo_test.cl"
                    , "line":107
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Floating Point to Integer Conversion"
                    , "data":
                    [232, 228, 0, 0]
                  }
                  , "count":2
                }
                , {
                  "info":
                  {
                    "name":"Fmul"
                    , "data":
                    [68, 96, 0, 2]
                  }
                  , "count":2
                }
                , {
                  "info":
                  {
                    "name":"Integer to Floating Point Conversion"
                    , "data":
                    [344, 514, 0, 0]
                  }
                  , "count":2
                }
              ]
            }
            , {
              "name":"yolo_test.cl:108"
              , "data":
              [32, 64, 0, 4]
              , "debug":
              [
                [
                  {
                    "filename":"c:/Users/admin/Desktop/New folder/yolofpga/yolo_test/device/yolo_test.cl"
                    , "line":108
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Mul"
                    , "data":
                    [0, 64, 0, 4]
                  }
                  , "count":2
                }
                , {
                  "info":
                  {
                    "name":"Sub"
                    , "data":
                    [32, 0, 0, 0]
                  }
                  , "count":2
                }
              ]
            }
            , {
              "name":"yolo_test.cl:109"
              , "data":
              [32, 64, 0, 4]
              , "debug":
              [
                [
                  {
                    "filename":"c:/Users/admin/Desktop/New folder/yolofpga/yolo_test/device/yolo_test.cl"
                    , "line":109
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Mul"
                    , "data":
                    [0, 64, 0, 4]
                  }
                  , "count":2
                }
                , {
                  "info":
                  {
                    "name":"Sub"
                    , "data":
                    [32, 0, 0, 0]
                  }
                  , "count":2
                }
              ]
            }
            , {
              "name":"yolo_test.cl:111"
              , "data":
              [64, 64, 0, 4]
              , "debug":
              [
                [
                  {
                    "filename":"c:/Users/admin/Desktop/New folder/yolofpga/yolo_test/device/yolo_test.cl"
                    , "line":111
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Add"
                    , "data":
                    [32, 0, 0, 0]
                  }
                  , "count":2
                }
                , {
                  "info":
                  {
                    "name":"Mul"
                    , "data":
                    [0, 64, 0, 4]
                  }
                  , "count":2
                }
                , {
                  "info":
                  {
                    "name":"Sub"
                    , "data":
                    [32, 0, 0, 0]
                  }
                  , "count":2
                }
              ]
            }
            , {
              "name":"yolo_test.cl:112"
              , "data":
              [64, 64, 0, 4]
              , "debug":
              [
                [
                  {
                    "filename":"c:/Users/admin/Desktop/New folder/yolofpga/yolo_test/device/yolo_test.cl"
                    , "line":112
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Add"
                    , "data":
                    [32, 0, 0, 0]
                  }
                  , "count":2
                }
                , {
                  "info":
                  {
                    "name":"Mul"
                    , "data":
                    [0, 64, 0, 4]
                  }
                  , "count":2
                }
                , {
                  "info":
                  {
                    "name":"Sub"
                    , "data":
                    [32, 0, 0, 0]
                  }
                  , "count":2
                }
              ]
            }
            , {
              "name":"yolo_test.cl:114"
              , "data":
              [110, 73, 0, 0]
              , "debug":
              [
                [
                  {
                    "filename":"c:/Users/admin/Desktop/New folder/yolofpga/yolo_test/device/yolo_test.cl"
                    , "line":114
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"'__acl_barrier' Function Call"
                    , "data":
                    [110, 73, 0, 0]
                  }
                  , "count":1
                }
              ]
            }
            , {
              "name":"yolo_test.cl:115"
              , "data":
              [144, 0, 0, 0]
              , "debug":
              [
                [
                  {
                    "filename":"c:/Users/admin/Desktop/New folder/yolofpga/yolo_test/device/yolo_test.cl"
                    , "line":115
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Integer Compare"
                    , "data":
                    [128, 0, 0, 0]
                  }
                  , "count":14
                }
                , {
                  "info":
                  {
                    "name":"Or"
                    , "data":
                    [16, 0, 0, 0]
                  }
                  , "count":14
                }
              ]
            }
            , {
              "name":"yolo_test.cl:119"
              , "data":
              [7018, 9556, 86, 8]
              , "debug":
              [
                [
                  {
                    "filename":"c:/Users/admin/Desktop/New folder/yolofpga/yolo_test/device/yolo_test.cl"
                    , "line":119
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Add"
                    , "data":
                    [64, 0, 0, 0]
                  }
                  , "count":4
                }
                , {
                  "info":
                  {
                    "name":"Load"
                    , "data":
                    [6890, 9428, 86, 0]
                    , "details":
                    [
                      "Load with a private 64 kilobit cache. Cache is not shared with any other load. It is flushed on kernel start. Use Dynamic Profiler to verify cache effectiveness. Other kernels should not be updating the data in global memory while this kernel is using it. Cache is created when memory access pattern is data-dependent or appears to be repetitive. Simplify access pattern or mark pointer as 'volatile' to disable generation of this cache."
                    ]
                  }
                  , "count":2
                }
                , {
                  "info":
                  {
                    "name":"Mul"
                    , "data":
                    [0, 128, 0, 8]
                  }
                  , "count":4
                }
                , {
                  "info":
                  {
                    "name":"Pointer Computation"
                    , "data":
                    [64, 0, 0, 0]
                  }
                  , "count":2
                }
              ]
            }
            , {
              "name":"yolo_test.cl:122"
              , "data":
              [110, 73, 0, 0]
              , "debug":
              [
                [
                  {
                    "filename":"c:/Users/admin/Desktop/New folder/yolofpga/yolo_test/device/yolo_test.cl"
                    , "line":122
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"'__acl_barrier' Function Call"
                    , "data":
                    [110, 73, 0, 0]
                  }
                  , "count":1
                }
              ]
            }
            , {
              "name":"yolo_test.cl:123"
              , "data":
              [3806, 4059, 43, 0]
              , "debug":
              [
                [
                  {
                    "filename":"c:/Users/admin/Desktop/New folder/yolofpga/yolo_test/device/yolo_test.cl"
                    , "line":123
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Add"
                    , "data":
                    [14, 0, 0, 0]
                  }
                  , "count":1
                }
                , {
                  "info":
                  {
                    "name":"Load"
                    , "data":
                    [3662, 3971, 43, 0]
                    , "details":
                    [
                      "Load with a private 64 kilobit cache. Cache is not shared with any other load. It is flushed on kernel start. Use Dynamic Profiler to verify cache effectiveness. Other kernels should not be updating the data in global memory while this kernel is using it. Cache is created when memory access pattern is data-dependent or appears to be repetitive. Simplify access pattern or mark pointer as 'volatile' to disable generation of this cache."
                    ]
                  }
                  , "count":1
                }
                , {
                  "info":
                  {
                    "name":"Pointer Computation"
                    , "data":
                    [32, 0, 0, 0]
                  }
                  , "count":2
                }
                , {
                  "info":
                  {
                    "name":"Store"
                    , "data":
                    [98, 88, 0, 0]
                    , "details":
                    [
                      "Stall-free write to memory declared on yolo_test.cl:32."
                    ]
                  }
                  , "count":1
                }
              ]
            }
            , {
              "name":"yolo_test.cl:124"
              , "data":
              [196, 176, 0, 0]
              , "debug":
              [
                [
                  {
                    "filename":"c:/Users/admin/Desktop/New folder/yolofpga/yolo_test/device/yolo_test.cl"
                    , "line":124
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Store"
                    , "data":
                    [196, 176, 0, 0]
                    , "details":
                    [
                      "Stall-free write to memory declared on yolo_test.cl:33."
                    ]
                  }
                  , "count":2
                }
              ]
            }
            , {
              "name":"yolo_test.cl:126"
              , "data":
              [110, 73, 0, 0]
              , "debug":
              [
                [
                  {
                    "filename":"c:/Users/admin/Desktop/New folder/yolofpga/yolo_test/device/yolo_test.cl"
                    , "line":126
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"'__acl_barrier' Function Call"
                    , "data":
                    [110, 73, 0, 0]
                  }
                  , "count":1
                }
              ]
            }
            , {
              "name":"yolo_test.cl:137"
              , "data":
              [5831, 1752, 0, 32]
              , "debug":
              [
                [
                  {
                    "filename":"c:/Users/admin/Desktop/New folder/yolofpga/yolo_test/device/yolo_test.cl"
                    , "line":137
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Fadd"
                    , "data":
                    [4512, 0, 0, 0]
                  }
                  , "count":32
                }
                , {
                  "info":
                  {
                    "name":"Fmul"
                    , "data":
                    [1088, 1536, 0, 32]
                  }
                  , "count":32
                }
                , {
                  "info":
                  {
                    "name":"Load"
                    , "data":
                    [231, 216, 0, 0]
                    , "details":
                    [
                      "Stall-free read from memory declared on yolo_test.cl:32."
                      , "Stall-free read from memory declared on yolo_test.cl:33."
                    ]
                  }
                  , "count":3
                }
              ]
            }
            , {
              "name":"yolo_test.cl:140"
              , "data":
              [110, 73, 0, 0]
              , "debug":
              [
                [
                  {
                    "filename":"c:/Users/admin/Desktop/New folder/yolofpga/yolo_test/device/yolo_test.cl"
                    , "line":140
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"'__acl_barrier' Function Call"
                    , "data":
                    [110, 73, 0, 0]
                  }
                  , "count":1
                }
              ]
            }
          ]
        }
      ]
    }
  ]
}
;var fileJSON=[{"index":0, "path":"c:/Users/admin/Desktop/New folder/yolofpga/yolo_test/device/yolo_test.cl", "name":"yolo_test.cl", "content":"#ifndef SIMD_WORK_ITEMS\012#define SIMD_WORK_ITEMS 2 // default value\012#endif\012\012#define BLOCK_SIZE 16\012\012__kernel\012__attribute((reqd_work_group_size(BLOCK_SIZE,BLOCK_SIZE,1)))\012__attribute((num_simd_work_items(SIMD_WORK_ITEMS)))\012void conv( // Input and output matrices\012                 __global float *restrict C,\012                 __global float *A,\012                 __global float *B,\012                 __global float *bias,\012                 __global float *mean,\012                 __global float *variance,\012                 int batch_control,\012                 int relu_control,\012				 float ratio,\012                 int kernel_size,\012                 float inv_kernel_size,\012                 float inv_input_w,\012                 float inv_B_col_ori,\012                 float inv_B_width,\012                 int stride,\012                 int padding,\012                 int padding_offset,\012				 \012                 // Widths of matrices.\012                 int A_width, int B_width, int B_row_ori, int B_col_ori, int input_h, int input_w)\012{\012    __local float A_local[BLOCK_SIZE][BLOCK_SIZE];\012    __local float B_local[BLOCK_SIZE][BLOCK_SIZE];\012    __local float mean_local;\012	  __local float bias_local;\012	  __local float variance_local;\012\012    // Block index\012    int block_x = get_group_id(0);\012    int block_y = get_group_id(1);\012\012    // Local ID index (offset within a block)\012    int local_x = get_local_id(0);\012    int local_y = get_local_id(1);\012\012    // Global ID index\012    int global_x = get_global_id(0);\012    int global_y = get_global_id(1);\012\012    // Compute loop bounds\012    int a_start = A_width * BLOCK_SIZE * block_y;\012    int a_end   = a_start + A_width - 1;\012    int b_start = BLOCK_SIZE * block_x;\012\012    float running_sum = 0.0f;\012    float value = 0.0f;\012\012    // Compute the matrix multiplication result for this output element. Each\012    // loop iteration processes one block of the matrix.\012    for (int a = a_start, b = b_start ;a <= a_end; a += BLOCK_SIZE, b += (BLOCK_SIZE * B_width))\012    {\012        // Load the matrices to local memory. Note that the (x, y) indices\012        // are swapped for A_local and B_local. This affects the reads from\012        // A_local and B_local below and result in more efficient hardware.\012        //\012        // This is actually an optimization that the compiler can perform,\012        // but is shown here for illustration purposes.\012\012		  int bbb = b + B_width * (local_y) + local_x;\012		  barrier(CLK_LOCAL_MEM_FENCE | CLK_GLOBAL_MEM_FENCE);\012		  \012		  int global_b_channel = 0;\012\012		  global_b_channel = (int)(bbb * inv_B_width);\012		  int global_b_overflow = 0;\012		  global_b_overflow = bbb - (global_b_channel)*B_width;\012		  barrier(CLK_LOCAL_MEM_FENCE | CLK_GLOBAL_MEM_FENCE);\012\012		  int offset = 0;\012		  offset = global_b_channel * padding_offset ;\012		  barrier(CLK_LOCAL_MEM_FENCE | CLK_GLOBAL_MEM_FENCE);\012\012		  int bbb_update = 0;\012		  bbb_update = bbb - offset;\012		  barrier(CLK_LOCAL_MEM_FENCE | CLK_GLOBAL_MEM_FENCE);\012\012		  int xw = 0;\012		  xw = (int)(bbb_update * inv_input_w);\012		  int xwh = 0;\012		  xwh = (int)(bbb_update * inv_B_col_ori);\012		  barrier(CLK_LOCAL_MEM_FENCE | CLK_GLOBAL_MEM_FENCE);\012\012		  int global_b_width =  0;\012		  global_b_width = bbb_update  - xw*input_w ;\012		  int global_b_height = 0;\012		  global_b_height = xw -  (xwh)  * input_h;\012		  barrier(CLK_LOCAL_MEM_FENCE | CLK_GLOBAL_MEM_FENCE);\012\012		  int im_c = 0;\012		  int im_row = 0;\012		  int im_col = 0;\012		  int aaa = 0;\012		  barrier( CLK_LOCAL_MEM_FENCE | CLK_GLOBAL_MEM_FENCE);\012\012		  aaa = (int)(global_b_channel * inv_kernel_size);\012\012		  im_c= (int) (aaa * inv_kernel_size);\012		  im_col= global_b_channel  - ( aaa * kernel_size);\012		  im_row = aaa  - (int)(aaa*inv_kernel_size) * (kernel_size) ;\012\012		  im_col = im_col + global_b_width*stride -padding;\012		  im_row = im_row + global_b_height*stride - padding;\012\012		  barrier(CLK_LOCAL_MEM_FENCE | CLK_GLOBAL_MEM_FENCE);\012		  if(im_row < 0 || im_col < 0 || im_row >= input_h || im_col >= input_w || global_b_channel < 0 ||global_b_channel >= B_row_ori || (global_b_overflow >= B_col_ori) || (global_b_overflow < 0)){\012        value = 0.0f;\012      }\012      else{\012        value = B[im_c*B_col_ori + im_row*input_w + im_col];\012      }\012      ///printf(\"%d %f\\n\",bbb,value);\012      barrier(CLK_LOCAL_MEM_FENCE | CLK_GLOBAL_MEM_FENCE);\012      A_local[local_y][local_x] = A[a + A_width * local_y + local_x];\012      B_local[local_x][local_y] = value;\012      // Wait for the entire block to be loaded.\012      barrier(CLK_LOCAL_MEM_FENCE);\012\012      // Do the dot product accumulation within this block. Fully unroll the loop.\012      // As a result of the swap of indices above, memory accesses to\012      // A_local and B_local are very efficient because each loop iteration\012      // accesses consecutive elements. This can be seen by unrolling the\012      // loop and analyzing the regions that are loaded:\012      //  A_local[local_y][0..BLOCK_SIZE-1] and\012      //  B_local[local_x][0..BLOCK_SIZE-1]\012      #pragma unroll\012      for (int k = 0; k < BLOCK_SIZE; ++k){\012        running_sum += A_local[local_y][k] * B_local[local_x][k];\012      }\012      // Wait for the block to be fully consumed before loading the next block.\012      barrier(CLK_LOCAL_MEM_FENCE);\012    }\012    //printf(\"%f\\n\",running_sum);\012    mean_local = mean[get_global_id(1)];\012    variance_local = variance[get_global_id(1)];\012    bias_local = bias[get_global_id(1)];\012    barrier(CLK_GLOBAL_MEM_FENCE);\012\012    if(batch_control == 1){\012      running_sum = (running_sum - mean_local)*variance_local;\012    }\012	  barrier(CLK_GLOBAL_MEM_FENCE);\012\012    running_sum = running_sum + bias_local;\012	  barrier(CLK_GLOBAL_MEM_FENCE);\012\012    if (relu_control == 1 && running_sum < 0) {\012      running_sum = (ratio) * running_sum;\012    }\012	barrier(CLK_GLOBAL_MEM_FENCE);\012    C[get_global_id(1) * get_global_size(0) + get_global_id(0)] = running_sum;\012}\012"}];