#include <math.h>


typedef struct image{
	int h;
	int w;
	int c;
	float *data;
} image;

typedef struct box{
	float x,y,w,h;
} box;

typedef struct sortable_box{
    int index;
    int classs;
    float **probs;
} sortable_bbox;

float get_pixel(image m, int w, int h, int c);
void set_pixel(image m, int w, int h, int c, float val);
void add_pixel(image m, int w, int h, int c, float val);
void free_image(image m);
void rgbgr_image(image im);
void im2col(float* data_im, int channels, int height,int width, int ksize, int stride, int pad, float* data_col);

image make_image(int w, int h, int c);

image resize_image(image im, int w, int h);
image load_image(char *filename, int w, int h, int c);
float im2col_get_pixel(float *im, int height, int width, int channels, int row, int col, int channel, int pad);
size_t kernel_memRd_global_dimension[3] = {1,1,1};
size_t kernel_memRd_local_dimension[3] = {1,1,1};
void forward_region_layer();

void softmax(float *softmax_input, int n, float temp, int stride, float *softmax_output);
void get_detection_boxes(float *out, int imw, int imh, int w, int h, float thresh, float **probs, box *boxes, int only_objectness);
box get_region_box_cpu(float *x, float *biases, int n, int index, int i, int j, int w, int h, int stride);
static inline float logistic_activate(float x) { return 1.0F / (1.0F + exp(-x)); }

int nms_comparator(const void *pa, const void *pb);
void do_nms_sort(box *boxes, float **probs, int total, int classes, float thresh);
float box_intersection(box a, box b);
float box_iou(box a, box b);
float overlap(float x1, float w1, float x2, float w2);
float box_union(box a, box b);
float get_color(int c, int x, int max);
void draw_detections(image im, int num, float thresh, box *boxes, float **probs, char **names, image **alphabet, int classes);
void draw_box_width(image a, int x1, int y1, int x2, int y2, int w, float r, float g, float b);
void draw_box(image a, int x1, int y1, int x2, int y2, float r, float g, float b);
image **load_alphabet();
image border_image(image a, int border);
float get_pixel_extend(image m, int x, int y, int c);
void fill_cpu(int N, float ALPHA, float *X, int INCX);
void embed_image(image source, image dest, int dx, int dy);
image get_label(image **characters, char *string, int size);
void composite_image(image source, image dest, int dx, int dy);
void save_image_jpg(image p, const char *name);
image copy_image(image p);
image get_label(image **characters, char *string, int size);
image tile_images(image a, image b, int dx);
void draw_label(image a, int r, int c, image label, const float *rgb);
int max_index(float *a, int n);
void activate_array(float *x, const int n);
void softmax_cpu(float *input, int n, int batch, int batch_offset, int groups, int group_offset, int stride, float temp, float *output);
int entry_index(int location, int entry);
void correct_region_boxes(box *boxes, int n, int w, int h, int netw, int neth, int relative);
image letterbox_image(image im, int w, int h);
void letterbox_image_into(image im, int w, int h, image boxed);
void fill_image(image m, float s);
image load_image_stb(char *filename, int channels);
void save_image_png(image im, const char *name);
void save_image(image im, const char *name);