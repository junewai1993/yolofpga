﻿#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <cstring>
#include <iostream>
#include <fstream>
#include <assert.h>
#include <float.h>

//OPENCL LIBRARY
#include <CL/opencl.h>
#include "AOCLUtils/aocl_utils.h"

#define VOC
// CNN network configuration file
#ifdef VOC
	#include "voc.h"
	#include "layer_config_voc.h"
	#define REGION_VAL 125
#else
	#include "coco.h"
	#include "layer_config_coco.h"
	#define REGION_VAL 425
#endif

#ifdef OPENCV
	// opencv library
	#include "stdafx.h"
	#include <opencv2/opencv.hpp>
	using namespace cv;
#else

#endif

// External Library
#include "image.h"

// CNN Configuration
#define LAYER_NUM 9
#define BLOCK_SIZE 16
#ifdef VOC
	#define CLASSNUM 20
#else
	#define CLASSNUM 80
#endif

#define THRESH     0.5
#define NMS        0.3
#define IMG_SIZE   416
#define CNN_GRID   13
#define BOUND_INFO 5
// Namespace
using namespace std;
using namespace aocl_utils;

enum config_item{
	layer_type,
	input_w, input_h, input_c,
	weight_w, weight_h, weight_c, bias_size,
	matrixA_row, matrixA_col, matrixB_row, matrixB_col, matrixC_row, matrixC_col, flagA, flagB,flagC,
	memrd_src,
	conv_out_w, conv_out_h, conv_out_c, conv_stride, conv_padding, relu, batch_norm,
	pool_on, pool_out_w, pool_out_h, pool_out_c, pool_size, pool_stride,
	memwr_dst,
	regional
};

// Network Configuration
unsigned layer_config_new[LAYER_NUM][NUM_CONFIG_ITEM];

// OpenCL kernel names
const char *knl_name_conv = "conv";
cl_event kernel_conv_event;
cl_kernel kernel_conv;

// OpenCL runtime configuration
cl_platform_id platform_id = NULL;
cl_device_id device = NULL;
cl_context context = NULL;
cl_program program = NULL;

// OpenCL Devices
cl_uint num_devices = 0;
scoped_array<cl_device_id> devices;

// OpenCL Command Queue
cl_command_queue queue_memRd = NULL;
cl_command_queue queue_conv  = NULL;
cl_command_queue queue_maxpool = NULL;

// OpenCL memory region
scoped_array<cl_mem> bias_buffer;
scoped_array<cl_mem> rolling_mean_buffer;
scoped_array<cl_mem> rolling_variance_buffer;

scoped_array<cl_mem> weights_buffer;
scoped_array<cl_mem> data_buffer_in;
scoped_array<cl_mem> conv_buffer_out;

cl_mem output_buffer;
cl_int status;

// Define file location for weights and input
#ifdef VOC
	char *weightFile = "tiny-yolo-voc.weights";
#else
	char *weightFile = "tiny-yolo-coco.weights";
#endif
char *imageFile ="dog.jpg";
char *PredictionImage = "image";

// Global variable to hold weight, bias temporary
float *weight_conv[LAYER_NUM];
float *bias_conv[LAYER_NUM];
float *scales[LAYER_NUM];
float *rolling_mean[LAYER_NUM];
float *rolling_variance[LAYER_NUM];
float *rolling_variance_update[LAYER_NUM];
unsigned buffer_size = 0;

// Function before executing fpga kernel
void parse_convolutional_layer_weight(char *filename);
char *get_layer_name(int layer_n, int properties);
void load_network();
void prepare_network();
bool init_opencl();

// Kernel Function (Initialization)
void forward_Conv_layer(int j, int weight_row, int weight_col, int data_row, int data_width);

// Clear all Allocated memory, buffer and OpenCL Command Queue
void cleanup();

// OpenCL helper functions
static void device_info_ulong( cl_device_id device, cl_device_info param, const char* name);
static void device_info_uint( cl_device_id device, cl_device_info param, const char* name);
static void device_info_bool( cl_device_id device, cl_device_info param, const char* name);
static void device_info_string( cl_device_id device, cl_device_info param, const char* name);
static void display_device_info( cl_device_id device );

// Im2col function
void im2col(int layer, float* data_im, int channels, int height,int width, int ksize, int stride, int pad, float* data_col);
float im2col_get_pixel(float *im, int height, int width, int channels, int row, int col, int channel, int pad);

// Last Layer - Region Layer
void forward_region_layer(int classes);
void softmax(float *softmax_input, int n, float temp, int stride, float *softmax_output);
void softmax_cpu(float *input, int n, int batch, int batch_offset, int groups, int group_offset, int stride, float temp, float *output);
int entry_index(int location, int entry);
void correct_region_boxes(box *boxes, int n, int w, int h, int netw, int neth, int relative);
box get_region_box_cpu(float *x, float *biases, int n, int index, int i, int j, int w, int h, int stride);
void get_detection_boxes(float *out, int imw, int imh, int w, int h, float thresh, float **probs, box *boxes, int only_objectness,int classes);

// Activation Function ( Logistic )
void activate_array(float *x, const int n);
static inline float logistic_activate(float x) { return 1.0F / (1.0F + exp(-x)); }
void activate_leakyarray(float *x, const int n);
static inline float leaky_activate(float x){return (x>0) ? x : .1*x;}
//maxpool
void forward_maxpool_layer(int b);

// To check the neccessary to add padding
int validatePaddingDataRow(int layer);
int validatePaddingDataCol(int layer);
int validatePaddingWeightRow(int layer);
int validatePaddingWeightCol(int layer);

// Add or Remove Padding to data and weight
void resizeMatrix(float *in, int rowOri, int colOri, int rowNew, int colNew, float *out);
void transpose(int h, int w, float *x);
void network_run(float *input_image);

//batch norm
void batchnorm(float *output, float *biases, float *scales, float *mean, float *variance, int row, int col, int batch_control, int relu_control);

// Global Variable
float *MatrixB;
float *MatrixC;
float *region_out;
float *MatrixC_pool;

int main(int argc, char** argv){

	// Initilize OPENCL
	if(!init_opencl()){
		return -1;
	}

	load_network();																	// Constructing CNN network and loading data
	for (int i = 0 ; i < 10 ; ++i ){								// Loading Anchor value
		anchor[i] = (float)anchor[i];
	}
	region_anchor = anchor;
	image **alphabet = load_alphabet();					// Loading Label Data - Alphabet
	parse_convolutional_layer_weight(weightFile);		// Loading Weight Data

	prepare_network();
	image ima = load_image(imageFile, 0, 0, 3);								// Loading Image ( Input )
	image sized = letterbox_image(ima,IMG_SIZE,IMG_SIZE);			// Resize Image to 416 x 416
	float *input_image = sized.data;

	network_run(input_image); 											//layer run + forward region layer

	box *boxes = (box *)calloc(CNN_GRID*CNN_GRID*BOUND_INFO, sizeof(box));
	float **probs = (float **)calloc(CNN_GRID*CNN_GRID*BOUND_INFO, sizeof(float *));
	for(unsigned int j = 0; j < CNN_GRID*CNN_GRID*BOUND_INFO; ++j) probs[j] = (float *)calloc(CLASSNUM, sizeof(float *));
	get_detection_boxes(region_out, ima.w,ima.h,IMG_SIZE,IMG_SIZE, THRESH, probs, boxes, 0, CLASSNUM);
	if (NMS) do_nms_sort( boxes , probs , CNN_GRID*CNN_GRID*BOUND_INFO , CLASSNUM, NMS );
	draw_detections(ima,CNN_GRID*CNN_GRID*BOUND_INFO , THRESH, boxes, probs, obj_names, alphabet, CLASSNUM);
	save_image(ima, PredictionImage);

	// Free allocated memory for image, resized image, boxes, probabilities and allocated buffers
	free_image(ima);
	free_image(sized);
	free(boxes);
	free(probs);
	cleanup();
}

void load_network(){
	for(unsigned int i = 0 ; i < LAYER_NUM ; ++i){
		for (unsigned int j = 0 ; j < NUM_CONFIG_ITEM ; ++j) {
			layer_config_new[i][j] = layer_config[i][j];
		}
	}

	// matrix A is weight matrix
	// matrix B is data matrix
	// Note: Matrix A width == Matrix B height, since not every weight & data dimension is divisible to BLOCK_SIZE,
	// 			 neccessary approach takes place to handle this
	for (unsigned int i = 0; i < LAYER_NUM; ++i) {

		// MATRIX A - WEIGHTS
		int weight_row_new = validatePaddingWeightRow(i);
		int weight_col_new = validatePaddingWeightCol(i);
		int weight_row_ori = layer_config[i][matrixA_row];
		int weight_col_ori = layer_config[i][matrixA_col];

		// MATRIX B - DATA / INPUT FEATURE
		int data_row_new = validatePaddingDataRow(i);
		int data_col_new = validatePaddingDataCol(i);
		int data_row_ori = layer_config[i][matrixB_row];
		int data_col_ori = layer_config[i][matrixB_col];

		// MATRIX C - OUTPUT
		int output_row_new = weight_row_new;
		int output_col_new = data_col_new;
		int output_row_ori = layer_config[i][matrixC_row];
		int output_col_ori = layer_config[i][matrixC_col];

		// UPDATE NETWORK CONFIGURATION
		layer_config_new[i][matrixA_row] = weight_row_new;
		layer_config_new[i][matrixA_col] = weight_col_new;
		layer_config_new[i][matrixB_row] = data_row_new;
		layer_config_new[i][matrixB_col] = data_col_new;
		layer_config_new[i][matrixC_row] = output_row_new;
		layer_config_new[i][matrixC_col] = output_col_new;

		// RAISE PADDING FLAG IF THE MATRIX A/B DIMENSION CHANGE
		if ((weight_row_new != weight_row_ori) || (weight_col_new != weight_col_ori)) {
			layer_config_new[i][flagA] = 1;
		}
		if ((data_row_new != data_row_ori) || (data_col_new != data_col_ori)) {
			layer_config_new[i][flagB] = 1;
		}
		if ((output_row_new != output_row_ori) || (output_col_new != output_col_ori)) {
			layer_config_new[i][flagC] = 1;
		}
	}
	// printf("...Constructing CNN Network - Tiny Yolov2\n");
	// printf("Layer	 Type		 Input			 Output\n");
	// printf("---------------------------------------------------------------------------------------------\n");
	for (unsigned int i = 0 ; i < LAYER_NUM ; ++i) {
	// fprintf(stderr, "Layer %d\t %s\t %d x %d x %d	 \t %d x %d x %d\n", i+1, get_layer_name(i,layer_type), layer_config_new[i][input_w], layer_config_new[i][input_h], layer_config_new[i][input_c], layer_config_new[i][conv_out_w], layer_config_new[i][conv_out_h], layer_config_new[i][conv_out_c]);
    }
}

char *get_layer_name(int layer_n, int properties){
    if(layer_config_new[layer_n][properties] == 0) return "ConvMaxpool";
    else return "Convolutional";
}
void parse_convolutional_layer_weight(char *filename){

    // fprintf(stderr, "Loading weights from %s\n", filename);
    fflush(stdout);

    FILE *fp = fopen(filename,"rb");
    if(!fp){
      // fprintf(stderr, "%s cannot be opened\n", filename);
    }
    // These four value major, minor, revision and seen are not going be used
    int major;
    int minor;
    int revision;
    int seen;

    fread(&major,sizeof(int),1,fp);
    fread(&minor,sizeof(int),1,fp);
    fread(&revision,sizeof(int),1,fp);
    fread(&seen,sizeof(int),1,fp);

	#ifndef VOC
		int lol;
		fread(&lol,sizeof(int),1,fp);
	#else
	#endif

	  // The desired data start reading here.
		for(unsigned int j = 0 ; j < LAYER_NUM ; ++j){

			int bias_allocate_size   = layer_config_new[j][matrixA_row];
			int weight_allocate_size = layer_config_new[j][matrixA_row] * layer_config_new[j][matrixA_col];

			int bias_read_size   = layer_config[j][matrixA_row];
			int weight_read_size = layer_config[j][matrixA_row] * layer_config[j][matrixA_col];

			// Allocate memory for bias, scales, rolling mean, variance and weight file
			bias_conv[j] 				= (float *) calloc(bias_allocate_size,  sizeof(float));
			scales[j] 					= (float *) calloc(bias_allocate_size,  sizeof(float));
			rolling_mean[j] 			= (float *) calloc(bias_allocate_size,  sizeof(float));
			rolling_variance[j]			= (float *) calloc(bias_allocate_size,  sizeof(float));
			rolling_variance_update[j] 	= (float *) calloc(bias_allocate_size, sizeof(float));
			weight_conv[j] 				= (float *) calloc(weight_allocate_size, sizeof(float));

			// Read weights, bias, scales, rolling mean and rolling variance from weight file
			fread(bias_conv[j],sizeof(float),bias_read_size,fp);
			if(layer_config_new[j][batch_norm]==1){
				fread(scales[j],sizeof(float),bias_read_size,fp);
				fread(rolling_mean[j],sizeof(float),bias_read_size,fp);
				fread(rolling_variance[j],sizeof(float),bias_read_size,fp);
				// DATA PREPROCESSING HERE
				// COMBINING BOTH SCALES AND ROLLING VARIANCE
				for(int i = 0 ; i < layer_config_new[j][bias_size]; ++i){
					rolling_variance_update[j][i] = (float) ((1.0f/(sqrt(rolling_variance[j][i]) + .000001f)) * scales[j][i]);
				}
			}
			fread(weight_conv[j],sizeof(float),weight_read_size,fp);
		}
		// CLOSE FILE
		fclose(fp);
		// FREE ALLOCATED MEMORY FOR SCALE & ROLLING_VARIANCE
		

		// CHECK THE WEIGHT DIMENSION, INSERT PADDING WHEN NECCESSARY
		for (unsigned int j = 0 ; j < LAYER_NUM ; ++j){
			if ( layer_config_new[j][flagA]) {
				// ADD PADDING TO WEIGHT
				float *temp = (float *)calloc( layer_config_new[j][matrixA_row] * layer_config_new[j][matrixA_col], sizeof(float));
				resizeMatrix(weight_conv[j], layer_config[j][matrixA_row], layer_config[j][matrixA_col], layer_config_new[j][matrixA_row], layer_config_new[j][matrixA_col],temp);
				(float *)memcpy(weight_conv[j], temp, layer_config_new[j][matrixA_row] * layer_config_new[j][matrixA_col] * sizeof(float));
				free(temp);
			}
		}
		// CHECK THE BIAS DIMENSION, INSERT PADDING WHEN NECCESSARY
		for (unsigned int j = 0 ; j < LAYER_NUM ; ++j){
			int bias_row_new = 1;
			int bias_col_new = layer_config_new[j][matrixA_row];
			int bias_row_ori = 1;
			int bias_col_ori = layer_config[j][matrixA_row];
			if (bias_col_new != bias_col_ori) {
				float *temp = (float *)calloc(bias_row_new * bias_col_new, sizeof(float));
				resizeMatrix(bias_conv[j],bias_row_ori,bias_col_ori,bias_row_new, bias_col_new,temp);
				(float *)memcpy(bias_conv[j], temp, bias_row_new * bias_col_new * sizeof(float));
				free(temp);
			}
		}
}
bool init_opencl(){
	cl_int status;

	// printf("...Initializing OpenCL FPGA SDK\n");

	if(!setCwdToExeDir()) {
    return false;
	}

	platform_id = findPlatform("Intel(R) FPGA SDK for OpenCL(TM)");

	if(platform_id == NULL){
		// printf("ERROR: Unable to find the desired OpenCL platform.\n");
		return false;
	}

	devices.reset(getDevices(platform_id, CL_DEVICE_TYPE_ALL, &num_devices));
	// printf("Platform: %s\n", getPlatformName(platform_id).c_str());
	// printf("Using %d device(s)\n", num_devices);
	for(unsigned i = 0; i < num_devices; ++i) {
		// printf("%s\n", getDeviceName(devices[i]).c_str());
	}

	device = devices[0];
	display_device_info(device);

	// CREATE CONTEXT
	context = clCreateContext(NULL,1,&device,NULL,NULL,&status);
	checkError(status, "Failed to create context");

	// CREATE PROGRAM
	std::string binary_file = getBoardBinaryFile("yolo_test", device);
	// printf("Using AOCX: %s\n", binary_file.c_str());
	program = createProgramFromBinary(context, binary_file.c_str(), &device, 1);

	// Build the program that was just created.
	status = clBuildProgram(program, 0, NULL, "", NULL, NULL);
	checkError(status, "Failed to build program");

	weights_buffer.reset(LAYER_NUM);
	data_buffer_in.reset(LAYER_NUM);
	conv_buffer_out.reset(LAYER_NUM);

	// bias, scale, mean and rolling
	bias_buffer.reset(LAYER_NUM);
	rolling_mean_buffer.reset(LAYER_NUM);
	rolling_variance_buffer.reset(LAYER_NUM);

	// Create command queue
	queue_conv = clCreateCommandQueue(context, device, CL_QUEUE_PROFILING_ENABLE, &status);
	checkError(status, "Failed to create command queue for queue_conv");

	kernel_conv  = clCreateKernel(program, knl_name_conv, &status);
	checkError(status, "Failed to create kernel kernel_conv");

	return true;
}

void prepare_network(){
	// Allocate the memory for the buffer for each layer
	for (unsigned int j = 0 ; j < LAYER_NUM ; ++j) {

		// Create Buffer for Matrix B - INPUT
		buffer_size = layer_config_new[j][input_c] * layer_config_new[j][input_h] * layer_config_new[j][input_w] * sizeof(float);
		data_buffer_in[j] = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_BANK_2_ALTERA, buffer_size, NULL, &status);
		checkError(status, "Failed to create buffer to data buffer in layer %d", j);

		// Create Buffer for Matrix C - Convolution Output
		buffer_size = layer_config_new[j][matrixC_row] * layer_config_new[j][matrixC_col] * sizeof(float);
		conv_buffer_out[j] = clCreateBuffer(context, CL_MEM_READ_WRITE  | CL_MEM_BANK_1_ALTERA, buffer_size, NULL, &status);
		checkError(status, "Failed to create buffer to conv buffer in layer %d", j);

		// Create Buffer for Weights & transfer the weight into created buffer
		buffer_size = layer_config_new[j][matrixA_row] * layer_config_new[j][matrixA_col] * sizeof(float);
		weights_buffer[j] = clCreateBuffer(context,CL_MEM_READ_ONLY  | CL_MEM_BANK_1_ALTERA,buffer_size, NULL,&status);
		checkError(status,"Failed to create buffer for weights in layer %d", j);
		status = clEnqueueWriteBuffer(queue_conv,weights_buffer[j],CL_TRUE,0,buffer_size,weight_conv[j],0,NULL,NULL);
		checkError(status,"Failed to transfer weights");

		// Take note: Buffer size of scale, mean and variance are same
		buffer_size = layer_config_new[j][matrixA_row] * sizeof(float);

		// Create buffer for bias & Transfer bias to kernel
		buffer_size = layer_config_new[j][matrixA_row] * sizeof(float);
		bias_buffer[j] = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_BANK_2_ALTERA, buffer_size, NULL, &status);
		checkError(status, "Failed to create buffer for bias in layer %d",j);
		status = clEnqueueWriteBuffer(queue_conv,bias_buffer[j],CL_TRUE,0,buffer_size,bias_conv[j],0,NULL,NULL);
		checkError(status,"Failed to transfer bias in layer %d",j);

		// Create buffer for rolling_mean & transfer rolling_mean to kernel
		rolling_mean_buffer[j] = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_BANK_2_ALTERA, buffer_size, NULL, &status);
		checkError(status, "Failed to create buffer for rolling mean in layer %d", j);
		status = clEnqueueWriteBuffer(queue_conv,rolling_mean_buffer[j],CL_TRUE,0,buffer_size,rolling_mean[j],0,NULL,NULL);
		checkError(status,"Failed to transfer rolling_mean in layer %d", j);

		// Create buffer for rolling_variance & transfer rolling_variance to kernel
		rolling_variance_buffer[j] = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_BANK_2_ALTERA, buffer_size, NULL, &status);
		checkError(status, "Failed to create buffer for rolling variance in layer %d", j);
		status = clEnqueueWriteBuffer(queue_conv,rolling_variance_buffer[j],CL_TRUE,0,buffer_size,rolling_variance_update[j],0,NULL,NULL);
		checkError(status,"Failed to transfer rolling_variance %d", j);
	}
}
void network_run(float *input_image){
	const double network_start_time = getCurrentTimestamp();
	for(unsigned int j = 0 ; j < LAYER_NUM ; ++j){
		 printf("\n******************************************     Layer %d     **************************\n\n\n",j+1);
		const double layer_start_time = getCurrentTimestamp();

		// START WRITE DATA TO BUFFER
		const double writing_buffer_data_start_time = getCurrentTimestamp();
				MatrixB = (float *)calloc( layer_config_new[j][input_c] * layer_config_new[j][input_h] *layer_config_new[j][input_w] ,sizeof(float));
				if( j == 0){
					status = clEnqueueWriteBuffer(queue_conv,data_buffer_in[j],CL_TRUE,0,layer_config_new[j][input_c] * layer_config_new[j][input_h] *layer_config_new[j][input_w]*sizeof(float),input_image,0,NULL,NULL);
					checkError(status, "Failed to transfer Feature %d layer", j);
				}
				else if( (j != 0) && (layer_config_new[j][layer_type] == 0)){
					status = clEnqueueWriteBuffer(queue_conv, data_buffer_in[j], CL_TRUE,0,layer_config_new[j][input_c] * layer_config_new[j][input_h] *layer_config_new[j][input_w]*sizeof(float),MatrixC,0,NULL,NULL);
					checkError(status, "Failed to transfer Feature %d layer", j);
					free(MatrixC);
				}
				else{
					status = clEnqueueWriteBuffer(queue_conv, data_buffer_in[j], CL_TRUE,0,layer_config_new[j][input_c] * layer_config_new[j][input_h] *layer_config_new[j][input_w]*sizeof(float),MatrixC,0,NULL,NULL);
					checkError(status, "Failed to transfer Feature %d layer", j);
					free(MatrixC);
				}
		// Wait for command queue to complete pending events
		clFinish(queue_conv);
		const double writing_buffer_data_end_time = getCurrentTimestamp();
		const double writing_buffer_data_total_time = writing_buffer_data_end_time - writing_buffer_data_start_time;
		free(MatrixB);

		// START CONVOLUTION
		const double convo_start_time = getCurrentTimestamp();
			forward_Conv_layer(j, layer_config_new[j][matrixA_row], layer_config_new[j][matrixA_col], layer_config_new[j][matrixB_row],layer_config_new[j][matrixB_col]);
			clFinish(queue_conv);
		const double convo_end_time = getCurrentTimestamp();
		const double convo_total_time = convo_end_time - convo_start_time;
		// Compute GFLOPs
		const float flops = (float)(2.0f * layer_config_new[j][matrixC_row] * layer_config_new[j][matrixC_col] * layer_config_new[j][matrixA_col] / convo_total_time);

		// START READING OUTPUT DATA
		const double reading_buffer_data_start_time = getCurrentTimestamp();
			MatrixC = (float *) calloc( layer_config_new[j][matrixC_row] * layer_config_new[j][matrixC_col],sizeof(float));
			status = clEnqueueReadBuffer(queue_conv, conv_buffer_out[j], CL_TRUE, 0, layer_config_new[j][matrixC_row] * layer_config_new[j][matrixC_col] * sizeof(float), MatrixC, 0, NULL, NULL);
			clFinish(queue_conv);
		const double reading_buffer_data_end_time = getCurrentTimestamp();
		const double reading_buffer_data_total_time = reading_buffer_data_end_time - reading_buffer_data_start_time;
	
		
		// batchnorm(MatrixC, bias_conv[j], scales[j], rolling_mean[j], rolling_variance_update[j], layer_config_new[j][matrixC_row], layer_config_new[j][matrixC_col], layer_config_new[j][batch_norm], layer_config_new[j][relu]);
		// printf("\n");
		//for(int i = 0 ; i < layer_config_new[j][matrixC_row] * layer_config_new[j][matrixC_col] ; i++){
		//	 printf("%f\n",MatrixC[i]);
		//}
		// printf("\n\n\n\n");
		// REMOVE PADDING FOR CONVO OUTPUT, THIS MIGHT NOT BE NECCESSARY
		// TEMPERORY LEAVE IT
		if(layer_config_new[j][flagC]){
			const double remove_padding_start_time = getCurrentTimestamp();
			float *temp = (float *)calloc(layer_config_new[j][matrixC_row] * layer_config_new[j][matrixC_col], sizeof(float));
			memcpy(temp,MatrixC, layer_config_new[j][matrixC_row] * layer_config_new[j][matrixC_col]*sizeof(float));
			resizeMatrix(temp, layer_config_new[j][matrixC_row], layer_config_new[j][matrixC_col], layer_config[j][matrixC_row], layer_config[j][matrixC_col], MatrixC);
			free(temp);
			const double remove_padding_end_time = getCurrentTimestamp();
			const double remove_padding_total_time = remove_padding_end_time - remove_padding_start_time;
		}

		// MAXPOOLING
		if(layer_config[j][pool_on]){
			const double maxpool_start_time = getCurrentTimestamp();
			MatrixC_pool = (float *) calloc(layer_config_new[j][pool_out_h] * layer_config_new[j][pool_out_w] * layer_config_new[j][pool_out_c],sizeof(float));
			forward_maxpool_layer(j);
			memcpy(MatrixC, MatrixC_pool, layer_config_new[j][pool_out_h] * layer_config_new[j][pool_out_w] * layer_config_new[j][pool_out_c] * sizeof(float));
			free(MatrixC_pool);
			const double maxpool_end_time = getCurrentTimestamp();
			const double maxpool_total_time = maxpool_end_time - maxpool_start_time;
		}

		const double layer_end_time = getCurrentTimestamp();
		const double layer_total_time = layer_end_time - layer_start_time;

		// DISPLAY TIME DETAILS
		// printf("\nOperation[Write Data]  Execution Time: %0.3f ms\n", writing_buffer_data_total_time * 1e3);
		// printf("\nOperation[ConvolutioN] Execution Time: %0.3f ms\n", convo_total_time * 1e3);
		// printf("\nOperation[Read Data] Execution Time: %0.3f ms\n", reading_buffer_data_total_time * 1e3);

		 printf("\nThroughput: %0.2f GFLOPS\n\n", flops * 1e-9);
		 printf("\nLayer[%d] Execution Time: %0.3f ms\n", j+1,layer_total_time * 1e3);

		 printf("******************************************       END       ****************************\n\n\n",j+1);
	}

	const double network_end_time = getCurrentTimestamp();
	const double network_total_time = network_end_time - network_start_time;
	// Wall-clock time taken.
	 printf("All %d layer is Successfully Executed", LAYER_NUM);
	 printf("\nNetwork Execution Time: %0.3f ms\n", network_total_time * 1e3);

	status = clFinish(queue_conv);
	checkError(status, "Failed to finish");

	//printf("Region Layer started");
	forward_region_layer(CLASSNUM);
	//printf("\nDone Region Layer\n");
}
void forward_Conv_layer(int j, int weight_row, int weight_width, int data_row, int data_width){

	unsigned argi = 0;
	status = clSetKernelArg(kernel_conv, argi++, sizeof(cl_mem), &conv_buffer_out[j]);
	checkError(status, "Failed to set argument %d of kernel conv", argi - 1);

	status = clSetKernelArg(kernel_conv, argi++, sizeof(cl_mem), &weights_buffer[j]);
	checkError(status, "Failed to set argument %d of kernel conv", argi - 1);

	status = clSetKernelArg(kernel_conv, argi++, sizeof(cl_mem), &data_buffer_in[j]);
	checkError(status, "Failed to set argument %d of kernel conv", argi - 1);

	status = clSetKernelArg(kernel_conv, argi++, sizeof(cl_mem), &bias_buffer[j]);
	checkError(status, "Failed to set argument %d of kernel conv", argi - 1);

	status = clSetKernelArg(kernel_conv, argi++, sizeof(cl_mem), &rolling_mean_buffer[j]);
	checkError(status, "Failed to set argument %d of kernel conv", argi - 1);

	status = clSetKernelArg(kernel_conv, argi++, sizeof(cl_mem), &rolling_variance_buffer[j]);
	checkError(status, "Failed to set argument %d of kernel conv", argi - 1);


	status = clSetKernelArg(kernel_conv, argi++, sizeof(layer_config_new[j][batch_norm]), &layer_config_new[j][batch_norm]);
	checkError(status, "Failed to set argument %d of kernel conv", argi - 1);

	status = clSetKernelArg(kernel_conv, argi++, sizeof(layer_config_new[j][relu]), &layer_config_new[j][relu]);
	checkError(status, "Failed to set argument %d of kernel conv", argi - 1);

	float ratio = 0.1f;
	status = clSetKernelArg(kernel_conv, argi++, sizeof(float), &ratio);
	checkError(status, "Failed to set argument %d of kernel conv", argi - 1);

	status = clSetKernelArg(kernel_conv, argi++, sizeof(layer_config_new[j][weight_h]), &layer_config_new[j][weight_h]);
	checkError(status, "Failed to set argument %d of kernel conv", argi - 1);

	float inv_kernel_size = 1.0f / layer_config_new[j][weight_h];
	status = clSetKernelArg(kernel_conv, argi++, sizeof(inv_kernel_size), &inv_kernel_size);
	checkError(status, "Failed to set argument %d of kernel conv", argi - 1);

	float inv_input_w = 1.0f / layer_config_new[j][input_w];
	float inv_B_col_ori = 1.0f / layer_config[j][matrixB_col];
	float inv_B_width = 1.0f / data_width;

	
	status = clSetKernelArg(kernel_conv, argi++, sizeof(inv_input_w), &inv_input_w);
	checkError(status, "Faild to set argument %d of kernel conv", argi - 1);

	status = clSetKernelArg(kernel_conv, argi++, sizeof(inv_B_col_ori), &inv_B_col_ori);
	checkError(status, "Failed to set argument %d of kernel conv", argi - 1);

	status = clSetKernelArg(kernel_conv, argi++, sizeof(inv_B_width), &inv_B_width);
	checkError(status, "Failed to set argument %d of kernel conv", argi - 1);

	status = clSetKernelArg(kernel_conv, argi++, sizeof(layer_config_new[j][conv_stride]), &layer_config_new[j][conv_stride]);
	checkError(status, "Failed to set argument %d of kernel conv", argi - 1);

	status = clSetKernelArg(kernel_conv, argi++, sizeof(layer_config_new[j][conv_padding]), &layer_config_new[j][conv_padding]);
	checkError(status, "Failed to set argument %d of kernel_conv", argi - 1);

	int padding_offset = layer_config_new[j][matrixB_col] - layer_config[j][matrixB_col];

	status = clSetKernelArg(kernel_conv, argi++, sizeof(padding_offset), &padding_offset);
	checkError(status, "Failed to set argument %d of kernel_conv", argi - 1);

	status = clSetKernelArg(kernel_conv, argi++, sizeof(weight_width), &weight_width);
	checkError(status, "Failed to set argument %d of kernel conv", argi - 1);

	status = clSetKernelArg(kernel_conv, argi++, sizeof(data_width), &data_width);
	checkError(status, "Failed to set argument %d of kernel conv", argi - 1);

	status = clSetKernelArg(kernel_conv, argi++, sizeof(layer_config[j][matrixB_row]), &layer_config[j][matrixB_row]);
	checkError(status, "Failed to set argument %d of kernel conv", argi - 1);

	status = clSetKernelArg(kernel_conv, argi++, sizeof(layer_config[j][matrixB_col]), &layer_config[j][matrixB_col]);
	checkError(status, "Failed to set argument %d of kernel conv", argi - 1);

	status = clSetKernelArg(kernel_conv, argi++, sizeof(layer_config_new[j][input_h]),&layer_config_new[j][input_h]);
	checkError(status, "Failed to set argument %d of kernel conv", argi - 1);

	status = clSetKernelArg(kernel_conv, argi++, sizeof(layer_config_new[j][input_w]),&layer_config_new[j][input_w]);
	checkError(status, "Failed to set argument %d of kernel conv", argi - 1);

	//printf("%d %d %d %d\n", weight_width, data_width, layer_config[j][matrixB_row], layer_config[j][matrixB_col], layer_config_new[j][input_h], layer_config_new[j][input_w]);

	const size_t global_work_size[2] = {data_width,weight_row};
	const size_t local_work_size[2]  = {BLOCK_SIZE, BLOCK_SIZE};

	//printf("Launching Convolution for layer %d (global size: %zu x %zu)\n", j, global_work_size[1], global_work_size[0]);

	status = clEnqueueNDRangeKernel(queue_conv, kernel_conv, 2, NULL, global_work_size, local_work_size, 0, NULL, &kernel_conv_event);
	checkError(status, "Failed to launch kernel");
}

// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// Clear all allocated buffer, memory and command queue
void cleanup() {
  if(kernel_conv){
	clReleaseKernel(kernel_conv);
  }
  int j = 0;
  for(j= 0 ; j < LAYER_NUM ; ++j){
	  if(weights_buffer[j])				clReleaseMemObject(weights_buffer[j]);
	  if(data_buffer_in[j])				clReleaseMemObject(data_buffer_in[j]);
	  if(conv_buffer_out[j])			clReleaseMemObject(conv_buffer_out[j]);
	  if(bias_buffer[j])					clReleaseMemObject(bias_buffer[j]);
	  if(rolling_mean_buffer[j])  clReleaseMemObject(rolling_mean_buffer[j]);
	  if(rolling_variance_buffer[j]) clReleaseMemObject(rolling_variance_buffer[j]);
  }

  if(output_buffer)		clReleaseMemObject(output_buffer);
  if(program)			clReleaseProgram(program);
  if(queue_conv)		clReleaseCommandQueue(queue_conv);
  if(context)			clReleaseContext(context);

  for(j = 0 ; j < LAYER_NUM ; ++j){
	  if (weight_conv[j]) 			free(weight_conv[j]);
	  if (bias_conv[j]) 				free(bias_conv[j]);
	  if (rolling_mean[j]) 			free(rolling_mean[j]);
	  if (scales[j]) 				free(scales[j]);
	  if (rolling_variance[j]) 			free(rolling_variance[j]);
	  if (rolling_variance_update[j]) 	free(rolling_variance_update[j]);
  }
   if(MatrixC)			   free(MatrixC);
}

// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// Query and display OpenCL information on device and runtime environment
static void display_device_info( cl_device_id device ) {

   //printf("Querying device for info:\n");
   //printf("========================\n");
   //device_info_string(device, CL_DEVICE_NAME, "CL_DEVICE_NAME");
   //device_info_string(device, CL_DEVICE_VENDOR, "CL_DEVICE_VENDOR");
   device_info_uint(device, CL_DEVICE_VENDOR_ID, "CL_DEVICE_VENDOR_ID");
   //device_info_string(device, CL_DEVICE_VERSION, "CL_DEVICE_VERSION");
   //device_info_string(device, CL_DRIVER_VERSION, "CL_DRIVER_VERSION");
   device_info_uint(device, CL_DEVICE_ADDRESS_BITS, "CL_DEVICE_ADDRESS_BITS");
   device_info_bool(device, CL_DEVICE_AVAILABLE, "CL_DEVICE_AVAILABLE");
   device_info_bool(device, CL_DEVICE_ENDIAN_LITTLE, "CL_DEVICE_ENDIAN_LITTLE");
   device_info_ulong(device, CL_DEVICE_GLOBAL_MEM_CACHE_SIZE, "CL_DEVICE_GLOBAL_MEM_CACHE_SIZE");
   device_info_ulong(device, CL_DEVICE_GLOBAL_MEM_CACHELINE_SIZE, "CL_DEVICE_GLOBAL_MEM_CACHELINE_SIZE");
   device_info_ulong(device, CL_DEVICE_GLOBAL_MEM_SIZE, "CL_DEVICE_GLOBAL_MEM_SIZE");
   device_info_bool(device, CL_DEVICE_IMAGE_SUPPORT, "CL_DEVICE_IMAGE_SUPPORT");
   device_info_ulong(device, CL_DEVICE_LOCAL_MEM_SIZE, "CL_DEVICE_LOCAL_MEM_SIZE");
   device_info_ulong(device, CL_DEVICE_MAX_CLOCK_FREQUENCY, "CL_DEVICE_MAX_CLOCK_FREQUENCY");
   device_info_ulong(device, CL_DEVICE_MAX_COMPUTE_UNITS, "CL_DEVICE_MAX_COMPUTE_UNITS");
   device_info_ulong(device, CL_DEVICE_MAX_CONSTANT_ARGS, "CL_DEVICE_MAX_CONSTANT_ARGS");
   device_info_ulong(device, CL_DEVICE_MAX_CONSTANT_BUFFER_SIZE, "CL_DEVICE_MAX_CONSTANT_BUFFER_SIZE");
   device_info_uint(device, CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS, "CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS");
   device_info_uint(device, CL_DEVICE_MEM_BASE_ADDR_ALIGN, "CL_DEVICE_MEM_BASE_ADDR_ALIGN");
   device_info_uint(device, CL_DEVICE_MIN_DATA_TYPE_ALIGN_SIZE, "CL_DEVICE_MIN_DATA_TYPE_ALIGN_SIZE");
   device_info_uint(device, CL_DEVICE_PREFERRED_VECTOR_WIDTH_CHAR, "CL_DEVICE_PREFERRED_VECTOR_WIDTH_CHAR");
   device_info_uint(device, CL_DEVICE_PREFERRED_VECTOR_WIDTH_SHORT, "CL_DEVICE_PREFERRED_VECTOR_WIDTH_SHORT");
   device_info_uint(device, CL_DEVICE_PREFERRED_VECTOR_WIDTH_INT, "CL_DEVICE_PREFERRED_VECTOR_WIDTH_INT");
   device_info_uint(device, CL_DEVICE_PREFERRED_VECTOR_WIDTH_LONG, "CL_DEVICE_PREFERRED_VECTOR_WIDTH_LONG");
   device_info_uint(device, CL_DEVICE_PREFERRED_VECTOR_WIDTH_FLOAT, "CL_DEVICE_PREFERRED_VECTOR_WIDTH_FLOAT");
   device_info_uint(device, CL_DEVICE_PREFERRED_VECTOR_WIDTH_DOUBLE, "CL_DEVICE_PREFERRED_VECTOR_WIDTH_DOUBLE");
   device_info_ulong(device, CL_DEVICE_MAX_MEM_ALLOC_SIZE, "CL_DEVICE_MAX_MEM_ALLOC_SIZE");

   {
      cl_command_queue_properties ccp;
      clGetDeviceInfo(device, CL_DEVICE_QUEUE_PROPERTIES, sizeof(cl_command_queue_properties), &ccp, NULL);
     // printf("%-40s = %s\n", "Command queue out of order? ", ((ccp & CL_QUEUE_OUT_OF_ORDER_EXEC_MODE_ENABLE)?"true":"false"));
      //printf("%-40s = %s\n", "Command queue profiling enabled? ", ((ccp & CL_QUEUE_PROFILING_ENABLE)?"true":"false"));
   }
}
// Helper functions to display parameters returned by OpenCL queries
static void device_info_ulong( cl_device_id device, cl_device_info param, const char* name) {
   cl_ulong a;
   clGetDeviceInfo(device, param, sizeof(cl_ulong), &a, NULL);
   //printf("%-40s = %lu\n", name, a);
}
static void device_info_uint( cl_device_id device, cl_device_info param, const char* name) {
   cl_uint a;
   clGetDeviceInfo(device, param, sizeof(cl_uint), &a, NULL);
   //printf("%-40s = %u\n", name, a);
}
static void device_info_bool( cl_device_id device, cl_device_info param, const char* name) {
   cl_bool a;
   clGetDeviceInfo(device, param, sizeof(cl_bool), &a, NULL);
   //printf("%-40s = %s\n", name, (a?"true":"false"));
}

// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// Im2Col Function
void im2col(int layer, float* data_im, int channels, int height,int width, int ksize, int stride, int pad, float* data_col){
			// Loop Counter
			int c, h, w;
			// The output height, width and channel, Refer darknet yolo for more info
			int height_col = (height + 2*pad - ksize) / stride + 1;
			int width_col = (width + 2*pad - ksize) / stride + 1;
			int channels_col = channels * ksize * ksize;

			int row_ori = channels_col;
			int col_ori = height_col * width_col;

 			// Starting Im2Col operation
			for (c = 0 ; c < channels_col ; ++c){
				// Computing offsets
				int w_offset = c % ksize;
				int h_offset = (c / ksize) % ksize;
				int c_im = c / ksize / ksize;
				for(h = 0 ; h < height_col ; ++h){
					for( w = 0 ; w < width_col ; ++w){
						int im_row = h_offset + h*stride;
						int im_col = w_offset + w*stride;
						int col_index = (c * height_col + h) * width_col + w;
						data_col[col_index] = im2col_get_pixel(data_im, height, width, channels, im_row, im_col, c_im, pad);
					}
				}
			}
}

float im2col_get_pixel(float *im, int height, int width, int channels,
                        int row, int col, int channel, int pad){
    row -= pad;
    col -= pad;

    if (row < 0 || col < 0 || row >= height || col >= width) return 0;
    return im[col + width*(row + height*channel)];

}

// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// Start from here is function to handle padding
int validatePaddingDataRow(int layer){
	int curr_height = layer_config[layer][matrixB_row];
	int new_h 			= ceil((float) curr_height / BLOCK_SIZE) * BLOCK_SIZE;
	return new_h;
}

int validatePaddingDataCol(int layer){
	int curr_width 	= layer_config[layer][matrixB_col];
	int new_w 			= ceil((float) curr_width / BLOCK_SIZE) * BLOCK_SIZE;
	return new_w;
}

int validatePaddingWeightRow(int layer){
	int curr_height = layer_config[layer][matrixA_row];
	int new_h 			= ceil((float) curr_height / BLOCK_SIZE) * BLOCK_SIZE;
	return new_h;
}

int validatePaddingWeightCol(int layer){
	int curr_width 	= layer_config[layer][matrixA_col];
	int new_w 			= ceil((float) curr_width / BLOCK_SIZE) * BLOCK_SIZE;
	return new_w;
}

void resizeMatrix(float *in, int rowOri, int colOri, int rowNew, int colNew, float *out) {
	if (rowOri > rowNew || colOri > colNew) {
		//remove Padding
		for (size_t i = 0; i < rowNew; i++){
			for (size_t j = 0; j < colNew; j++)
			{
				float value = 0.0f;
				if (i >= rowNew || i < 0 || j >= colNew || j < 0)	{}
				else{value = in[i*colOri + j];}
				out[i*colNew + j] = value;}
		}
	}

	else if (rowOri < rowNew || colOri < colNew){
		//add Padding
		for (size_t i = 0; i < rowNew; i++){
			for (size_t j = 0; j < colNew; j++){
				float value = 0.0f;
				if (i >= rowOri || i < 0 || j >= colOri || j < 0){ value = 0.0f;}
				else{ value = in[i*colOri + j];}
				out[i*colNew + j] = value;
			}
		}
	}
}

void transpose(int h, int w, float *x){
	int i, j;
	float *temp = (float *) calloc(h*w, sizeof(float));
	(float *)memcpy(temp,x,h*w*sizeof(float));
	//#pragma omp parallel for
	for ( i = 0; i < h ; ++i){
		for( j = 0 ; j < w ; ++j){
			x[j*h + i] = temp[i*w + j];
		}
	}
	free(temp);
}

/*
	!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	WE SHOULD ADD THE MAXPOOL FUNCTION OVER HERE TEMPERORY
*/
void forward_maxpool_layer(int b){
    int i,j,k,m,n;

    int h = layer_config[b][pool_out_h];
    int w = layer_config[b][pool_out_w];
    int c = layer_config[b][pool_out_c];

        for(k = 0; k < c; ++k){
            for(i = 0; i < h; ++i){
                for(j = 0; j < w; ++j){
                    int out_index = j + w*(i + h*k);
                    float max = -FLT_MAX;
                    int max_i = -1;
                    for(n = 0; n <  layer_config[b][pool_size]; ++n){
                        for(m = 0; m <  layer_config[b][pool_size]; ++m){
                            int cur_h =  i*layer_config[b][pool_stride] + n;
                            int cur_w =  j*layer_config[b][pool_stride] + m;
                            int index = cur_w + layer_config[b][conv_out_w]*(cur_h + layer_config[b][conv_out_h]*k);
                            int valid = (cur_h >= 0 && cur_h <layer_config[b][conv_out_h] &&
                                         cur_w >= 0 && cur_w < layer_config[b][conv_out_w]);
                            float val = (valid != 0) ? MatrixC[index] : -FLT_MAX;
                            max_i = (val > max) ? index : max_i;
                            max   = (val > max) ? val   : max;
                        }
                    }
                    MatrixC_pool[out_index] = max;
                }
            }
        }
}

/* Starting From Here
	All functions below are used in Last Layer -- Region Layer
	The coordinate of bbox will be computed here
*/
int entry_index(int location, int entry){
    int n =   location / (13*13);
    int loc = location % (13*13);
    return n*13*13*(CLASSNUM + BOUND_INFO) + entry*13*13 + loc;
}

// Activate function
void activate_array(float *x, const int n){
    int i;
    for(i = 0; i < n; ++i){
        x[i] = logistic_activate(x[i]);
    }
}

void activate_leakyarray(float *x, const int n){
    int i;
    for(i = 0; i < n; ++i){
        x[i] = leaky_activate(x[i]);
    }
}

void forward_region_layer(int classes){
	int coords = 4;
	//int classes = 20;
	int size = coords+classes+1;
	region_out = (float *)calloc( CNN_GRID * CNN_GRID * REGION_VAL,sizeof(float));
	memcpy(region_out, MatrixC, CNN_GRID * CNN_GRID * REGION_VAL * sizeof(float));
	int n;
	for( n = 0 ; n < 5 ; ++n){
		int index = entry_index(n * CNN_GRID * CNN_GRID,0);
		activate_array(region_out + index , 2 * CNN_GRID * CNN_GRID);
		index = entry_index(n * CNN_GRID * CNN_GRID , 4);
		activate_array(region_out+index, CNN_GRID * CNN_GRID);
	}
	int index = entry_index(0 , BOUND_INFO);
	softmax_cpu(MatrixC + index, classes , BOUND_INFO , (CNN_GRID * CNN_GRID * REGION_VAL) / BOUND_INFO, CNN_GRID * CNN_GRID , 1, CNN_GRID * CNN_GRID, 1, region_out + index);
}

// Softmax Function
void softmax(float *softmax_input, int n, float temp, int stride, float *softmax_output){
	int i;
	float sum = 0;
	float largest = - FLT_MAX;
	for ( i = 0; i < n; ++i){
		if (softmax_input[i*stride] > largest) largest = softmax_input[i];
	}
	for( i = 0 ; i < n; ++i){
		float e = exp(softmax_input[i*stride] / temp - largest / temp);
		sum += e;
		softmax_output[i*stride] = e;
	}

	for( i = 0 ; i < n; ++i){
		softmax_output[i*stride]/=sum;
	}
}
void softmax_cpu(float *input, int n, int batch, int batch_offset, int groups, int group_offset, int stride, float temp, float *output){
    int g, b;
    for(b = 0; b < batch; ++b){
        for(g = 0; g < groups; ++g){
            softmax(input + b*batch_offset + g*group_offset, n, temp, stride, output + b*batch_offset + g*group_offset);
        }
    }
}

//get_region_boxes(l, im.w, im.h, net->w, net->h, thresh, probs, boxes, masks, 0, 0, hier_thresh, 1);
void get_detection_boxes(float *outre, int w, int h, int imw, int imh, float thresh, float **probs, box *boxes, int only_objectness,int classes){
	int i, j, n;
	float *predictions = outre;
	for( i = 0 ; i < CNN_GRID * CNN_GRID; ++i){
		int row = i / CNN_GRID;
		int col = i % CNN_GRID;
		// anchor index
		for (n = 0; n < BOUND_INFO; ++n) {
			int index = n * CNN_GRID * CNN_GRID + i;	// index for each grid-cell & anchor
			for(j = 0 ; j < classes ; ++j){
				probs[index][j] = 0;
			}
			int obj_index = entry_index(n * CNN_GRID * CNN_GRID + i ,4);
			int box_index = entry_index(n * CNN_GRID * CNN_GRID + i ,0);
			float scale = predictions[obj_index];

			boxes[index] = get_region_box_cpu(predictions, region_anchor, n, box_index, col, row, CNN_GRID , CNN_GRID, CNN_GRID * CNN_GRID);

			float max = 0;
			for( j = 0; j < classes ; ++j){
				int class_index = entry_index(n * CNN_GRID * CNN_GRID + i, 4 + 1 + j);
				float prob = scale * predictions[class_index];
				probs[index][j] = (prob > thresh)? prob:0;
				if(prob > max ) max = prob;
			}
			probs[index][classes] = max;
		}
	}
	correct_region_boxes(boxes, CNN_GRID * CNN_GRID *BOUND_INFO, w,h, imw, imh, 1);
}

//box, 13*13*125, image widgth, image height, 1
void correct_region_boxes(box *boxes, int n, int w, int h, int netw, int neth, int relative){
    int i;
    int new_w=0;
    int new_h=0;
    if (((float)netw/w) < ((float)neth/h)) {
        new_w = netw;
        new_h = (h * netw)/w;
    } else {
        new_h = neth;
        new_w = (w * neth)/h;
    }
    for (i = 0; i < n; ++i){
        box b = boxes[i];
        b.x =  (b.x - (netw - new_w)/2./netw) / ((float)new_w/netw);
        b.y =  (b.y - (neth - new_h)/2./neth) / ((float)new_h/neth);
        b.w *= (float)netw/new_w;
        b.h *= (float)neth/new_h;
        if(!relative){
            b.x *= w;
            b.w *= w;
            b.y *= h;
            b.h *= h;
        }
        boxes[i] = b;
    }
}

box get_region_box_cpu(float *x, float *biases, int n, int index, int i, int j, int w, int h,int stride){
	box b;
	b.x = ( i + x[index + 0*stride]) / w;					// (col + 1./(1. + exp(-x))) / width_last_layer
	b.y = ( j + x[index + 1*stride]) / h;					// (row + 1./(1. + exp(-x))) / height_last_layer
	b.w = exp(x[index + 2*stride]) * biases[2 * n] / w;		// exp(x) * anchor_w / width_last_layer
	b.h = exp(x[index + 3*stride]) * biases[2 * n + 1] / h;	// exp(x) * anchor_h / height_last_layer
	return b;
}

void batchnorm(float *output, float *biases, float *scales, float *mean, float *variance, int row, int col, int batch_control, int relu_control){
	int i, j;
	float value;
	for( i = 0 ; i < row ; ++i){
		for( j = 0 ; j < col ; ++j){
			value = output[i * col + j];
			if(batch_control == 1){
				value = ((value - mean[i]) * variance[i]);
			}
			value = value + biases[i];
			if (relu_control == 1){
				value = leaky_activate(value);
			}
			output[i * col + j] = value;
		}
	}
}
