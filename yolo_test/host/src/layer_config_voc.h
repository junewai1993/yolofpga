// #include "../device/hw_param.cl"

// Explanation: The following config_item is the config format of yolo config file, we have several important configurations as following
//					- layer type
//					- input size
//					- weight size
//					- memory reading source
//					- Convolutional configuration
//					- pooling configuration
//					- memory write destination
//					- region layer configuration
// Details:
// Layer type:
//			0 representing convolutional layer with maxpooling
//			1 representing convolutional layer without maxpooling
//			Note: Not every convolutional layer is followed with maxpooling
// Input size:
//			input_w representing input feature width
//			input_h representing input feature height
//			input_c representing input feature channels
//			Note: This also representing the input image size for the first layer
// weight size:
//			weight_w representing weight width
//			weight_h representing weight height
//			weight_c representing weight channels
//			bias_size representing number of bias, scales, rolling mean and rolling variance
//			Note: bias_size also representing the 4th dimension of weight, and number of filter with the size weight_w * weight_h * weight_c;
// memrd_src:
//			0 representing data fetch from data buffer
//			1 representing data fetch from output buffer
//			Note: This configuration is controlling the data flow, it will work together with memwr_dst
// convolutional layer configuration:
//			conv_out_w representing the convolutional output width
//			conv_out_h representing the convolutional output height
//			conv_out_c representing the convolutional output channels
//			conv_stride representing the stride used in convolution
//			conv_padding representing the padding parameter, 1->padding, 0->no padding
//			relu representing the activation used in convolution, 1->leaky, 0->linear
//			batch_norm representing the batch_norm parameter, 1->batchnorm, 0->no batchnorm
//			Note: All layers in YOLO is using the Leaky activation (exept the last layer: using Linear)
// pooling layer configuration
//			pool_on representing whether maxpool is followed by convolution, 1->maxpool, 0->no maxpool
//			pool_out_w representing the maxpool output width
//			pool_out_h representing the maxpool output height
//			pool_out_c representing the maxpool output channels
//			pool_size representing the maxpool window size
//			pool_stride representing the maxpool stride
// memwr_dst
//			0 representing data fetch to data buffer
//			1 representing data fetch to output buffer
//			Note: This configuration is controlling the data flow, it will work together with memrd_src
// region layer configuration
//			0 representing no region
//			1 representing region

#define NUM_CONFIG_ITEM 33

unsigned layer_config[][NUM_CONFIG_ITEM] = {
//Layer 1
							{0, // layer type 0->convmax 1->conv
							416, 416, 3, //input size
							3, 3, 3, 16, // filter size , biases number
							16, 3*3*3, 3*3*3, 416*416, 16, 416 * 416, 0, 0, 0, //matrix A, matrix B, flags
							0, // data src "0"-> data_buf  "1"-> output_buf  "2"->"fc_1_buffer"  "3"->"fc_2_buffer"
							416, 416, 16, 1, 1, 1, 1,// ,conv output, stride, pad, relu 1->leaky 0->linear , batch norm? 1-> yes 0-> no
							1, 208, 208, 16, 2, 2, //pool? 0->bypass 1->maxpool // pooloutput size x,y,z, size, stride
							1, // data dst "0"-> data_buf  "1"-> output_buf  "2"->"fc_1_buffer"  "3"->"fc_2_buffer"
							0},//Layer-1 0->	 no region		 1-> region
// Layer 2
							{0,
							208,208,16, //input size
							3, 3, 16, 32, // filter size x,y,c, biases number
							32, 3*3*16, 3*3*16, 208*208, 32, 208*208, 0, 0, 0,//matrix A, matrix B, flags
							0,
							208, 208, 32, 1, 1, 1, 1,//conv output, stride, pad, relu 1->leaky 0->linear , batch norm? 1-> yes 0-> no
							1, 104, 104, 32, 2, 2, //pool? 0->bypass 1->maxpool // pooloutput size x,y,z, size, stride
							1,
							0},//Layer-2 0->	 no region		 1-> region
// Layer 3
							{0,
							104,104,32, //input size
							3, 3, 32, 64, // filter size x,y,c, biases number
							64, 3*3*32, 3*3*32, 104*104, 64, 104*104, 0, 0, 0,//matrix A, matrix B, flags
							0,
							104, 104, 64, 1, 1, 1, 1,//conv output, stride, pad, relu 1->leaky 0->linear , batch norm? 1-> yes 0-> no
							1, 52, 52, 64, 2, 2, //pool? 0->bypass 1->maxpool // pooloutput size x,y,z, size, stride
							1,
							0},//Layer-3  0->	 no region		 1-> region
// Layer 4
							{0,
							52,52,64, //input size
							3, 3, 64, 128, // filter size x,y,c, biases number
							128, 3*3*64, 3*3*64, 52*52, 128, 52*52, 0, 0, 0,//matrix A, matrix B, flags
							0,
							52, 52, 128, 1, 1, 1, 1, //conv output, stride, pad, relu 1->leaky 0->linear , batch norm? 1-> yes 0-> no
							1, 26, 26, 128, 2, 2, //pool? 0->bypass 1->maxpool // pooloutput size x,y,z, size, stride
							1,
							0},//Layer-4 0->	 no region		 1-> region
// Layer 5
							{0,
							26,26,128,//input size
							3, 3, 128, 256, // filter size x,y,c, biases number
							256, 3*3*128, 3*3*128, 26*26, 256, 26*26, 0, 0, 0,//matrix A, matrix B, flags
							0,
							26, 26, 256, 1, 1, 1, 1, //conv output, stride, pad, relu 1->leaky 0->linear , batch norm? 1-> yes 0-> no
							1, 13, 13, 256, 2, 2, //pool? 0->bypass 1->maxpool // pooloutput size x,y,z, size, stride
							1,
							0},//Layer-5 0->	 no region		 1-> region
// Layer 6
							{0,
							13,13,256, //input size
							3, 3, 256, 512, // filter size x,y,c, biases number
							512, 3*3*256,3*3*256, 13*13, 512, 13*13, 0, 0, 0,//matrix A, matrix B, flags
							0,
							13, 13, 512, 1, 1, 1, 1, //conv output, stride, pad, relu 1->leaky 0->linear , batch norm? 1-> yes 0-> no
							1, 13, 13, 512, 2, 1, //pool? 0->bypass 1->maxpool // pooloutput size x,y,z, size, stride
							1,
							0},//Layer-6
// Layer 7
							{1,
							13, 13, 512,//input size
							3, 3, 512, 1024, // filter size x,y,c, biases number
							1024, 3*3*512, 3*3*512, 13*13, 1024, 13*13, 0, 0, 0,//matrix A, matrix B, flags
							0,
							13, 13, 1024, 1, 1, 1, 1, //conv output, stride, pad, relu 1->leaky 0->linear , batch norm? 1-> yes 0-> no
							0, 13, 13, 1024, 0, 0, //pool? 0->bypass 1->maxpool // pooloutput size x,y,z, size, stride
							1,
							0},//Layer-7
// Layer 8
							{1,
							13, 13, 1024, //input size
							3, 3, 1024, 1024, // filter size x,y,c, biases number
							1024, 3*3*1024, 3*3*1024, 13*13, 1024, 13*13, 0, 0, 0,//matrix A, matrix B, flags
							0,
							13, 13, 1024, 1, 1, 1, 1, //conv output, stride, pad, relu 1->leaky 0->linear , batch norm? 1-> yes 0-> no
							0, 13, 13, 1024, 0, 0, //pool? 0->bypass 1->maxpool // pooloutput size x,y,z, size, stride
							1,
							0},//Layer-8
// Layer 9
							{1,
							13, 13, 1024, //input size
							1, 1, 1024, 125, // filter size x,y,c, biases number
							125, 1*1*1024, 1*1*1024,13*13, 125, 13*13, 0, 0, 0,//matrix A, matrix B, flags
							0,
							13, 13, 125, 1, 0, 0, 0, //conv output, stride, pad, relu 1->leaky 0->linear , batch norm? 1-> yes 0-> no
							0, 13, 13, 125, 0, 0, //pool? 0->bypass 1->maxpool // pooloutput size x,y,z, size, stride
							1,
							1} //Layer-9 //1 - > go to region kernel
							};

unsigned input_config[5] = {416, 416, 3, 1}; //original image size(dim1, dim2, dim3), batch size

unsigned output_config[3] = {13, 13, 125};//Layer-9

float anchor[10] = {1.08 , 1.19, 3.42, 4.41, 6.63, 11.38, 9.42, 5.11, 16.62, 10.52};

float *region_anchor;
