#ifndef IMAGE_H
#define IMAGE_H

#include <stdio.h>
#include <stdlib.h>

// Define image structure
typedef struct image{
	int h;
	int w;
	int c;
	float *data;
} image;

// Image Helper: Create Image, Resize Image, Save Image and Free Image
image make_image(int w, int h, int c);
image resize_image(image im, int w, int h);
image load_image(char *filename, int w, int h, int c);
void save_image(image im, const char *name);
void free_image(image m);

// Image Helper: Get Image Pixel, Modify Image Pixel, Set Image Pixel
float get_pixel(image m, int w, int h, int c);
void add_pixel(image m, int w, int h, int c, float val);
void set_pixel(image m, int w, int h, int c, float val);

// If OpenCV is supported, we will use OpenCV Image function
// Else, we will stick back using STB_IMAGE
#ifdef OPENCV
	image ipl_to_image(IplImage* src);
	image load_image_cv(char *filename, int channels);
	void rgbgr_image(image im);
	void ipl_into_image(IplImage* src, image im);
	void save_image_jpg(image p, const char *name);
#else
	void save_image_png(image im, const char *name);
	image load_image_stb(char *filename, int channels);
#endif

// Resize Image Helper
image copy_image(image p);
image letterbox_image(image im, int w, int h);
void letterbox_image_into(image im, int w, int h, image boxed);
void fill_image(image m, float s);
void embed_image(image source, image dest, int dx, int dy);

// Load the label alphabet
image **load_alphabet();


// Structure for a bounding box, storing coordinates
typedef struct box{
	float x,y,w,h;
} box;

// Structure for a sortable_box, storing class and probability
typedef struct sortable_box{
    int index;
    int classs;
    float **probs;
} sortable_bbox;

// All these functions used to execute the region layer
// To detect the object
// To predict the location
// To insert the labeling
// To draw the bounding box
int nms_comparator(const void *pa, const void *pb);
void do_nms_sort(box *boxes, float **probs, int total, int classes, float thresh);
float box_intersection(box a, box b);
float box_iou(box a, box b);
float overlap(float x1, float w1, float x2, float w2);
float box_union(box a, box b);
float get_color(int c, int x, int max);
void draw_detections(image im, int num, float thresh, box *boxes, float **probs, char **names, image **alphabet, int classes);
void draw_box_width(image a, int x1, int y1, int x2, int y2, int w, float r, float g, float b);
void draw_box(image a, int x1, int y1, int x2, int y2, float r, float g, float b);

image border_image(image a, int border);
float get_pixel_extend(image m, int x, int y, int c);
void fill_cpu(int N, float ALPHA, float *X, int INCX);
image get_label(image **characters, char *string, int size);
void composite_image(image source, image dest, int dx, int dy);
image get_label(image **characters, char *string, int size);
image tile_images(image a, image b, int dx);
void draw_label(image a, int r, int c, image label, const float *rgb);

void draw_label(image a, int r, int c, image label, const float *rgb);
void activate_array(float *x, const int n);
void softmax_cpu(float *input, int n, int batch, int batch_offset, int groups, int group_offset, int stride, float temp, float *output);
int entry_index(int location, int entry);
void correct_region_boxes(box *boxes, int n, int w, int h, int netw, int neth, int relative);


#endif