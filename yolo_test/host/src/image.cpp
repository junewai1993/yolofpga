#include "image.h"
	//stb library
	#define STB_IMAGE_IMPLEMENTATION
	#define STB_IMAGE_WRITE_IMPLEMENTATION
	#include "stb_image.h"
	#include "stb_image_write.h"

image make_image(int w, int h, int c){
	image out;
	out.data = 0;
	out.w = w;
	out.h = h;
	out.c = c;
	out.data = (float *) calloc(h*w*c, sizeof(float));
	return out;
}

image load_image(char *filename, int w, int h, int c){
	//printf("channels: %d",c);
#ifdef OpenCV
	image out = load_image_cv(filename,c);
#else
	image out = load_image_stb(filename,c);
#endif

	if ((h && w) && (h!=out.h || w!=out.w)){
		image resized = resize_image(out,w,h);
		free_image(out);
		out = resized;
	}
	return out;
}

image resize_image(image im, int w, int h){
	image resized = make_image(w,h,im.c);
	image part = make_image(w,im.h, im.c);

	int r,c,k;

	float w_scale = (float)(im.w - 1)/(w - 1);
	float h_scale = (float)(im.h - 1)/(h - 1);

	for( k = 0; k < im.c ; ++k){
		for( r = 0; r < im.h ; ++r){
			for( c = 0 ; c < w ; ++c){
				float val = 0;
				if (c == w-1 || im.w == 1){
					val = get_pixel(im, im.w -1, r, k);
				}
				else{
					float sx = c*w_scale;
					int ix = (int) sx;
					float dx = sx - ix;
					val = (1-dx)*get_pixel(im,ix,r,k) + dx*get_pixel(im,ix+1,r,k);
				}
				set_pixel(part,c,r,k,val);
			}
		}
	}
	for( k = 0 ; k < im.c ; ++k){
		for( r = 0 ; r < h ; ++r){
			float sy = r*h_scale;
			int iy = (int) sy;
			float dy = sy - iy;

			for( c = 0 ; c < w ; ++c){
				float val = (1-dy) * get_pixel(part,c,iy,k);
				set_pixel(resized, c, r, k, val);
			}
			if( r == h-1 || im.h == 1) continue;

			for( c = 0; c < w; ++c){
				float val = dy*get_pixel(part,c,iy+1,k);
				add_pixel(resized, c, r, k, val);
			}
		}
	}
	free_image(part);
	return resized;
}

float get_pixel(image m, int w, int h, int c){
	//assert(w < m.w && h < m.h && c < m.c);
	return m.data[c*m.h*m.w + h*m.w + w];
}

void set_pixel(image m, int w, int h, int c, float val){
	if (w < 0 || h < 0 || c < 0 || w >= m.w || h >= m.h || c >= m.c) return;
	//assert( w < m.w && h < m.h ; c < m.c);
	m.data[c*m.h*m.w + h*m.w + w] = val;
}

void add_pixel(image m, int w, int h, int c, float val){
	//assert( x < m.w && y < m.h && c < m.c);
	m.data[c*m.h*m.w + h*m.w + w] += val;
}

void save_image(image im, const char *name){
	#ifdef OPENCV
		save_image_jpg(im,name);
	#else
		save_image_png(im,name);
	#endif
}

void free_image(image m){
    if(m.data){
        free(m.data);
    }
}

#ifdef OPENCV
	void rgbgr_image(image im){
		int i;
		for (i = 0; i < im.w*im.h; ++i){
			float swap = im.data[i];
			im.data[i] = im.data[i+im.w*im.h*2];
			im.data[i+im.w*im.h*2] = swap;
		}
	}

	void save_image_jpg(image p, const char *name)
	{
		image copy = copy_image(p);
		if(p.c == 3) rgbgr_image(copy);
		int x,y,k;

		char buff[256];
		sprintf(buff, "%s.jpg", name);

		IplImage *disp = cvCreateImage(cvSize(p.w,p.h), IPL_DEPTH_8U, p.c);
		int step = disp->widthStep;
		for(y = 0; y < p.h; ++y){
			for(x = 0; x < p.w; ++x){
				for(k= 0; k < p.c; ++k){
					disp->imageData[y*step + x*p.c + k] = (unsigned char)(get_pixel(copy,x,y,k)*255);
				}
			}
		}
		cvSaveImage(buff, disp,0);
		cvReleaseImage(&disp);
		free_image(copy);
	}
	void ipl_into_image(IplImage* src, image im){
		unsigned char *data = (unsigned char *)src->imageData;
		int h = src->height;
		int w = src->width;
		int c = src->nChannels;
		int step = src->widthStep;
		int i,j,k;

		for(i = 0 ; i < h; ++i ){
			for(k=0 ; k < c; ++k){
				for(j=0; j < w; ++j){
					//printf("%u ",data[i*step + j*c +k]);
					im.data[k*w*h + i*w + j] = (float)data[i*step + j*c +k]/255;
					// printf("%f ",im.data[k*w*h + i*w + j]);
				}
			}
		}
	}

	image ipl_to_image(IplImage* src){
		int h = src->height;
		int w = src->width;
		int c = src->nChannels;
		image out = make_image(w,h,c);
		ipl_into_image(src,out);
		return out;
	}

	image load_image_cv(char *filename, int channels){
		IplImage* src = 0;
		int flag = -1;
		if (channels == 0) flag = -1;
		else if (channels == 1) flag = 0;
		else if (channels == 3) flag = 1;
		else{
			fprintf(stderr, "OpenCV can't force load with %d channels\n", channels);
		}

		if ((src = cvLoadImage(filename,flag)) == 0){
			fprintf(stderr, "Cannot load image \"%s\"\n", filename);
			char buff[256];
			sprintf(buff, "echo %s >> bad.list", filename);
			system(buff);
			return make_image(10,10,3);
		}

		image out = ipl_to_image(src);
		cvReleaseImage(&src);
		rgbgr_image(out);
		return out;
	}
#else
	void save_image_png(image im, const char *name){
		char buff[256];

		sprintf(buff, "%s.png", name);
		unsigned char *data = (unsigned char *)calloc(im.w*im.h*im.c,sizeof(char));
		int i,k;
		for(k=0; k < im.c ; ++k){
			for(i=0; i < im.w*im.h ; ++i){
				data[i*im.c+k] = (unsigned char)(255*im.data[i+k*im.h*im.w]);
			}
		}
		int success = stbi_write_png(buff,im.w,im.h,im.c,data,im.w*im.c);
		free(data);
		if(!success) fprintf(stderr, "Failed to write image %s\n",buff);
	}

	image load_image_stb(char *filename, int channels){
		int w, h, c;
		unsigned char *data = stbi_load(filename, &w, &h, &c, channels);
		if (!data) {
			fprintf(stderr, "Cannot load image \"%s\"\nSTB Reason: %s\n", filename, stbi_failure_reason());
			exit(0);
		}
		if(channels) c = channels;
		int i,j,k;
		image im = make_image(w, h, c);
		for(k = 0; k < c; ++k){
			for(j = 0; j < h; ++j){
				for(i = 0; i < w; ++i){
					int dst_index = i + w*j + w*h*k;
					int src_index = k + c*i + c*w*j;
					im.data[dst_index] = (float)data[src_index]/255.;
				}
			}
		}
		free(data);
		return im;
	}
#endif

image copy_image(image p)
{
    image copy = p;
    copy.data = (float *)calloc(p.h*p.w*p.c, sizeof(float));
    memcpy(copy.data, p.data, p.h*p.w*p.c*sizeof(float));
    return copy;
}

void letterbox_image_into(image im, int w, int h, image boxed)
{
    int new_w = im.w;
    int new_h = im.h;
    if (((float)w/im.w) < ((float)h/im.h)) {
        new_w = w;
        new_h = (im.h * w)/im.w;
    } else {
        new_h = h;
        new_w = (im.w * h)/im.h;
    }
    image resized = resize_image(im, new_w, new_h);
    embed_image(resized, boxed, (w-new_w)/2, (h-new_h)/2);
    free_image(resized);
}

image letterbox_image(image im, int w, int h)
{
    int new_w = im.w;
    int new_h = im.h;
    if (((float)w/im.w) < ((float)h/im.h)) {
        new_w = w;
        new_h = (im.h * w)/im.w;
    } else {
        new_h = h;
        new_w = (im.w * h)/im.h;
    }
    image resized = resize_image(im, new_w, new_h);
    image boxed = make_image(w, h, im.c);
    fill_image(boxed, .5);
    embed_image(resized, boxed, (w-new_w)/2, (h-new_h)/2);
    free_image(resized);
    return boxed;
}

void fill_image(image m, float s)
{
    int i;
    for(i = 0; i < m.h*m.w*m.c; ++i) m.data[i] = s;
}

void embed_image(image source, image dest, int dx, int dy)
{
    int x,y,k;
    for(k = 0; k < source.c; ++k){
        for(y = 0; y < source.h; ++y){
            for(x = 0; x < source.w; ++x){

                float val = get_pixel(source, x,y,k);

                set_pixel(dest, dx+x, dy+y, k, val);
            }
        }
    }
}

// To load the labeling alphabet from data folder
image **load_alphabet(){
    int i, j;
	// There's 8 different set sizes of alphabet
	// Each set has 127 characters (actually only 96 characters, starting from 32 - 127)
    const int nsize = 8;
	const int csize = 127;

	// Allocate memory for each set of alphabet
    image **alphabets = (image **)calloc(nsize, sizeof(image));

	for(j = 0; j < nsize; ++j){
		// Allocate memory for each characters
        alphabets[j] = (image *)calloc(128, sizeof(image));
        for(i = 32; i < csize; ++i){
			// Allocate memory for the label picture name, by default 256 in lengths
            char buff[256];
            sprintf(buff, "data/labels/%d_%d.png", i, j);
			// Load all the label into memory
            alphabets[j][i] = load_image(buff, 0, 0,3);
        }
    }
    return alphabets;
}

//

int nms_comparator(const void *pa, const void *pb)
{
    sortable_bbox a = *(sortable_bbox *)pa;
    sortable_bbox b = *(sortable_bbox *)pb;
    float diff = a.probs[a.index][b.classs] - b.probs[b.index][b.classs];
    if(diff < 0) return 1;
    else if(diff > 0) return -1;
    return 0;
}

void do_nms_sort(box *boxes, float **probs, int total, int classes, float thresh)
{
    int i, j, k;
    sortable_bbox *s = (sortable_bbox *)calloc(total, sizeof(sortable_bbox));

    for(i = 0; i < total; ++i){
        s[i].index = i;
        s[i].classs = 0;
        s[i].probs = probs;
    }

    for(k = 0; k < classes; ++k){
        for(i = 0; i < total; ++i){
            s[i].classs = k;
        }
        qsort(s, total, sizeof(sortable_bbox), nms_comparator);
        for(i = 0; i < total; ++i){
            if(probs[s[i].index][k] == 0) continue;
            box a = boxes[s[i].index];
            for(j = i+1; j < total; ++j){
                box b = boxes[s[j].index];
                if (box_iou(a, b) > thresh){
                    probs[s[j].index][k] = 0;
                }
            }
        }
    }
    free(s);
}

float box_intersection(box a, box b){
    float w = overlap(a.x, a.w, b.x, b.w);
    float h = overlap(a.y, a.h, b.y, b.h);
    if(w < 0 || h < 0) return 0;
    float area = w*h;
    return area;
}

float box_iou(box a, box b){
    return box_intersection(a, b)/box_union(a, b);
}

float overlap(float x1, float w1, float x2, float w2)
{
    float l1 = x1 - w1/2;
    float l2 = x2 - w2/2;
    float left = l1 > l2 ? l1 : l2;
    float r1 = x1 + w1/2;
    float r2 = x2 + w2/2;
    float right = r1 < r2 ? r1 : r2;
    return right - left;
}

float box_union(box a, box b)
{
    float i = box_intersection(a, b);
    float u = a.w*a.h + b.w*b.h - i;
    return u;
}

float get_color(int c, int x, int max)
{
	static float colors[6][3] = { { 1,0,1 },{ 0,0,1 },{ 0,1,1 },{ 0,1,0 },{ 1,1,0 },{ 1,0,0 } };
	float ratio = ((float)x / max) * 5;
	int i = floor(ratio);
	int j = ceil(ratio);
	ratio -= i;
	float r = (1 - ratio) * colors[i][c] + ratio*colors[j][c];
	//printf("%f\n", r);
	return r;
}

int max_index(float *a, int n)
{
    if(n <= 0) return -1;
    int i, max_i = 0;
    float max = a[0];
    for(i = 1; i < n; ++i){
        if(a[i] > max){
            max = a[i];
            max_i = i;
        }
    }
    return max_i;
}

void draw_detections(image im, int num, float thresh, box *boxes, float **probs, char **names, image **alphabet, int classes)
{
    int i,j;
    for(i = 0; i < num; ++i){
        char labelstr[4096] = {0};
		int classs = -1;
        for(j = 0; j < classes; ++j){
			//printf("%g ", probs[i][j]);
            if (probs[i][j] > thresh){
                if (classs < 0) {
                    strcat(labelstr, names[j]);
                    classs = j;
                } else {
                    strcat(labelstr, ", ");
                    strcat(labelstr, names[j]);
                }
                printf("%s: %.0f%%\n", names[j], probs[i][j]*100);
            }
        }
        if(classs >= 0){
        int width = im.h * .006;
            //printf("%d %s: %.0f%%\n", i, names[class], prob*100);
            int offset = classs*123457 % classes;
            float red = get_color(2,offset,classes);
            float green = get_color(1,offset,classes);
            float blue = get_color(0,offset,classes);
            float rgb[3];

            rgb[0] = red;
            rgb[1] = green;
            rgb[2] = blue;
            box b = boxes[i];

            int left  = (b.x-b.w/2.)*im.w;
            int right = (b.x+b.w/2.)*im.w;
            int top   = (b.y-b.h/2.)*im.h;
            int bot   = (b.y+b.h/2.)*im.h;

            if(left < 0) left = 0;
            if(right > im.w-1) right = im.w-1;
            if(top < 0) top = 0;
            if(bot > im.h-1) bot = im.h-1;

            draw_box_width(im, left, top, right, bot, width, red, green, blue);
            if (alphabet) {
                image label = get_label(alphabet, labelstr, (im.h*.03)/10);
                draw_label(im, top + width, left, label, rgb);
                free_image(label);
            }
        }
    }
}

image get_label(image **characters, char *string, int size)
{
    if(size > 7) size = 7;
    image label = make_image(0,0,0);
    while(*string){
        image l = characters[size][(int)*string];
        image n = tile_images(label, l, -size - 1 + (size+1)/2);
        free_image(label);
        label = n;
        ++string;
    }
    image b = border_image(label, label.h*.25);
    free_image(label);
    return b;
}

void draw_label(image a, int r, int c, image label, const float *rgb)
{
    int w = label.w;
    int h = label.h;
    if (r - h >= 0) r = r - h;

    int i, j, k;
    for(j = 0; j < h && j + r < a.h; ++j){
        for(i = 0; i < w && i + c < a.w; ++i){
            for(k = 0; k < label.c; ++k){
                float val = get_pixel(label, i, j, k);
                set_pixel(a, i+c, j+r, k, rgb[k] * val);
            }
        }
    }
}
image tile_images(image a, image b, int dx)
{
    if(a.w == 0) return copy_image(b);
    image c = make_image(a.w + b.w + dx, (a.h > b.h) ? a.h : b.h, (a.c > b.c) ? a.c : b.c);
    fill_cpu(c.w*c.h*c.c, 1, c.data, 1);
    embed_image(a, c, 0, 0);
    composite_image(b, c, a.w + dx, 0);
    return c;
}

void fill_cpu(int N, float ALPHA, float *X, int INCX)
{
    int i;
    for(i = 0; i < N; ++i) X[i*INCX] = ALPHA;
}

void composite_image(image source, image dest, int dx, int dy)
{
    int x,y,k;
    for(k = 0; k < source.c; ++k){
        for(y = 0; y < source.h; ++y){
            for(x = 0; x < source.w; ++x){
                float val = get_pixel(source, x, y, k);
                float val2 = get_pixel_extend(dest, dx+x, dy+y, k);
                set_pixel(dest, dx+x, dy+y, k, val * val2);
            }
        }
    }
}

float get_pixel_extend(image m, int x, int y, int c){
    if(x < 0 || x >= m.w || y < 0 || y >= m.h) return 0;
    if(c < 0 || c >= m.c) return 0;
    return get_pixel(m, x, y, c);
}

image border_image(image a, int border){
    image b = make_image(a.w + 2*border, a.h + 2*border, a.c);
    int x,y,k;
    for(k = 0; k < b.c; ++k){
        for(y = 0; y < b.h; ++y){
            for(x = 0; x < b.w; ++x){
                float val = get_pixel_extend(a, x - border, y - border, k);
                if(x - border < 0 || x - border >= a.w || y - border < 0 || y - border >= a.h) val = 1;
                set_pixel(b, x, y, k, val);
            }
        }
    }
    return b;
}

void draw_box_width(image a, int x1, int y1, int x2, int y2, int w, float r, float g, float b)
{
    int i;
    for(i = 0; i < w; ++i){
        draw_box(a, x1+i, y1+i, x2-i, y2-i, r, g, b);
    }
}

void draw_box(image a, int x1, int y1, int x2, int y2, float r, float g, float b){
    int i;
    if(x1 < 0) x1 = 0;
    if(x1 >= a.w) x1 = a.w-1;
    if(x2 < 0) x2 = 0;
    if(x2 >= a.w) x2 = a.w-1;

    if(y1 < 0) y1 = 0;
    if(y1 >= a.h) y1 = a.h-1;
    if(y2 < 0) y2 = 0;
    if(y2 >= a.h) y2 = a.h-1;

    for(i = x1; i <= x2; ++i){
        a.data[i + y1*a.w + 0*a.w*a.h] = r;
        a.data[i + y2*a.w + 0*a.w*a.h] = r;

        a.data[i + y1*a.w + 1*a.w*a.h] = g;
        a.data[i + y2*a.w + 1*a.w*a.h] = g;

        a.data[i + y1*a.w + 2*a.w*a.h] = b;
        a.data[i + y2*a.w + 2*a.w*a.h] = b;
    }
    for(i = y1; i <= y2; ++i){
        a.data[x1 + i*a.w + 0*a.w*a.h] = r;
        a.data[x2 + i*a.w + 0*a.w*a.h] = r;

        a.data[x1 + i*a.w + 1*a.w*a.h] = g;
        a.data[x2 + i*a.w + 1*a.w*a.h] = g;

        a.data[x1 + i*a.w + 2*a.w*a.h] = b;
        a.data[x2 + i*a.w + 2*a.w*a.h] = b;
    }
}
