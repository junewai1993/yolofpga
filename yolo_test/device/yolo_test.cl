#ifndef SIMD_WORK_ITEMS
#define SIMD_WORK_ITEMS 4 // default value
#endif

#define BLOCK_SIZE 4

__kernel
__attribute((reqd_work_group_size(BLOCK_SIZE,BLOCK_SIZE,1)))
__attribute((num_simd_work_items(SIMD_WORK_ITEMS)))
void conv( // Input and output matrices
                 __global float *restrict C,
                 __global float *A,
                 __global float *B,
                 __global float *bias,
                 __global float *mean,
                 __global float *variance,
                 int batch_control,
                 int relu_control,
				 float ratio,
                 int kernel_size,
                 float inv_kernel_size,
                 float inv_input_w,
                 float inv_B_col_ori,
                 float inv_B_width,
                 int stride,
                 int padding,
                 int padding_offset,
				 
                 // Widths of matrices.
                 int A_width, int B_width, int B_row_ori, int B_col_ori, int input_h, int input_w)
{
    __local float A_local[BLOCK_SIZE][BLOCK_SIZE];
    __local float B_local[BLOCK_SIZE][BLOCK_SIZE];
    __local float mean_local;
	  __local float bias_local;
	  __local float variance_local;

    // Block index
    int block_x = get_group_id(0);
    int block_y = get_group_id(1);

    // Local ID index (offset within a block)
    int local_x = get_local_id(0);
    int local_y = get_local_id(1);

    // Global ID index
    int global_x = get_global_id(0);
    int global_y = get_global_id(1);

    // Compute loop bounds
    int a_start = A_width * BLOCK_SIZE * block_y;
    int a_end   = a_start + A_width - 1;
    int b_start = BLOCK_SIZE * block_x;

    float running_sum = 0.0f;
    float value = 0.0f;

    // Compute the matrix multiplication result for this output element. Each
    // loop iteration processes one block of the matrix.
    for (int a = a_start, b = b_start ;a <= a_end; a += BLOCK_SIZE, b += (BLOCK_SIZE * B_width))
    {
        // Load the matrices to local memory. Note that the (x, y) indices
        // are swapped for A_local and B_local. This affects the reads from
        // A_local and B_local below and result in more efficient hardware.
        //
        // This is actually an optimization that the compiler can perform,
        // but is shown here for illustration purposes.

		  int bbb = b + B_width * (local_y) + local_x;
		  barrier(CLK_LOCAL_MEM_FENCE | CLK_GLOBAL_MEM_FENCE);
		  
		  int global_b_channel = 0;

		  global_b_channel = (int)(bbb * inv_B_width);
		  int global_b_overflow = 0;
		  global_b_overflow = bbb - (global_b_channel)*B_width;
		  barrier(CLK_LOCAL_MEM_FENCE | CLK_GLOBAL_MEM_FENCE);

		  int offset = 0;
		  offset = global_b_channel * padding_offset ;
		  barrier(CLK_LOCAL_MEM_FENCE | CLK_GLOBAL_MEM_FENCE);

		  int bbb_update = 0;
		  bbb_update = bbb - offset;
		  barrier(CLK_LOCAL_MEM_FENCE | CLK_GLOBAL_MEM_FENCE);

		  int xw = 0;
		  xw = (int)(bbb_update * inv_input_w);
		  int xwh = 0;
		  xwh = (int)(bbb_update * inv_B_col_ori);
		  barrier(CLK_LOCAL_MEM_FENCE | CLK_GLOBAL_MEM_FENCE);

		  int global_b_width =  0;
		  global_b_width = bbb_update  - xw*input_w ;
		  int global_b_height = 0;
		  global_b_height = xw -  (xwh)  * input_h;
		  barrier(CLK_LOCAL_MEM_FENCE | CLK_GLOBAL_MEM_FENCE);

		  int im_c = 0;
		  int im_row = 0;
		  int im_col = 0;
		  int aaa = 0;
		  barrier( CLK_LOCAL_MEM_FENCE | CLK_GLOBAL_MEM_FENCE);

		  aaa = (int)(global_b_channel * inv_kernel_size);

		  im_c= (int) (aaa * inv_kernel_size);
		  im_col= global_b_channel  - ( aaa * kernel_size);
		  im_row = aaa  - (int)(aaa*inv_kernel_size) * (kernel_size) ;

		  im_col = im_col + global_b_width*stride -padding;
		  im_row = im_row + global_b_height*stride - padding;

		  barrier(CLK_LOCAL_MEM_FENCE | CLK_GLOBAL_MEM_FENCE);
		  if(im_row < 0 || im_col < 0 || im_row >= input_h || im_col >= input_w || global_b_channel < 0 ||global_b_channel >= B_row_ori || (global_b_overflow >= B_col_ori) || (global_b_overflow < 0)){
        value = 0.0f;
      }
      else{
        value = B[im_c*B_col_ori + im_row*input_w + im_col];
      }
      ///printf("%d %f\n",bbb,value);
      barrier(CLK_LOCAL_MEM_FENCE | CLK_GLOBAL_MEM_FENCE);
      A_local[local_y][local_x] = A[a + A_width * local_y + local_x];
      B_local[local_x][local_y] = value;
      // Wait for the entire block to be loaded.
      barrier(CLK_LOCAL_MEM_FENCE);

      // Do the dot product accumulation within this block. Fully unroll the loop.
      // As a result of the swap of indices above, memory accesses to
      // A_local and B_local are very efficient because each loop iteration
      // accesses consecutive elements. This can be seen by unrolling the
      // loop and analyzing the regions that are loaded:
      //  A_local[local_y][0..BLOCK_SIZE-1] and
      //  B_local[local_x][0..BLOCK_SIZE-1]
      #pragma unroll
      for (int k = 0; k < BLOCK_SIZE; ++k){
        running_sum += A_local[local_y][k] * B_local[local_x][k];
      }
      // Wait for the block to be fully consumed before loading the next block.
      barrier(CLK_LOCAL_MEM_FENCE);
    }
    //printf("%f\n",running_sum);
    mean_local = mean[get_global_id(1)];
    variance_local = variance[get_global_id(1)];
    bias_local = bias[get_global_id(1)];
    barrier(CLK_GLOBAL_MEM_FENCE);

    if(batch_control == 1){
      running_sum = (running_sum - mean_local)*variance_local;
    }
	  barrier(CLK_GLOBAL_MEM_FENCE);

    running_sum = running_sum + bias_local;
	  barrier(CLK_GLOBAL_MEM_FENCE);

    if (relu_control == 1 && running_sum < 0) {
      running_sum = (ratio) * running_sum;
    }
	barrier(CLK_GLOBAL_MEM_FENCE);
    C[get_global_id(1) * get_global_size(0) + get_global_id(0)] = running_sum;
}


